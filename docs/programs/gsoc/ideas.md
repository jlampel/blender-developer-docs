# Blender Summer of Code 2025 - Ideas

This page collects potential ideas and mentors for contributor projects
in the summer of 2025.

## Contacting us

You can contact us via our [developers
forum](https://devtalk.blender.org/c/contributing-to-blender/summer-of-code/15) or on
[chat.blender.org](https://chat.blender.org/#/room/#gsoc-2025:blender.org).
For matters you prefer to discuss in private, mail Thomas Dinges: thomas
at blender.org.

## General information for contributors and mentors

New to GSoC? Learn [how it works](https://summerofcode.withgoogle.com/how-it-works/) on the GSoC website.

The ideas list below provides ideas for projects that:

- Have an expected project size of ~90, ~175 or ~350 working
  hours.
- Implements a feature that developers have agreed would be a great
  improvement for Blender.

> INFO: **Submit your own proposal**
> Choosing an idea from this list is *not* mandatory. Contributors are
> encouraged to submit their own proposals or modified versions of the
> ideas from this page. To get an idea of what sort of project can be done for Blender and of
> the current development direction, see:
>
> - [Code Blog](https://code.blender.org/)
> - Design and todo tasks listed in the Workboards of [Blender modules](https://projects.blender.org/)
> - GSoC projects from previous years: [2024](2024.md) - [2023](2023.md) - [2022](2022.md) - [2021](2021.md) - [2020](2020.md) - [2019](2019.md)
> - [Release Notes](../../release_notes/index.md)

### Mentors

A mentor should be someone with Blender coding experience, who can guide
a contributor during all stages of the work, from design to
implementation.

### Contributors

Contributors who wish to apply should first carefully read the [Getting
Started page](getting_started.md) and check if all the
conditions to participate are met.

If by some reason you can not participate, your contribution outside of
the program is still welcome! Feel free to develop an idea from the page
as long as no contributor chose it.

We especially invite contributors to contribute based on their past
experience and competencies, or based on their current research.

Nearly all project ideas will require a strong working knowledge of C
and/or C++. In addition, we use Python for much of our interface code
(anyone skilled in C/C++ can trivially learn enough Python to make the
needed interface code changes). An exception to this are projects where
only Python is required.

> INFO: **Get Started**
>
> [:octicons-arrow-right-24: Getting Started Guide and Application Template](getting_started.md)


## Ideas

<!---
Template for new project ideas

### Category

#### Project Title

- *Description:* XXX
- *Expected outcomes:* XXX
- *Skills required:* XXX
- *Possible mentors:* XXX
- *Expected project size:* 90, 175 or 350 hours
- *Difficulty:* easy, medium or hard
-->

### Flamenco

[Flamenco](https://flamenco.blender.org/) is Blender's render farm management system. For all the ideas below, a requirement is that you've set up Flamenco for yourself and used it to render things. Of course also some experience with Blender itself is needed.

#### Statistics

- *Description*: Design & build a system for Flamenco to collect and display statistics.
  - The goal is to show per-job, per-task, and per-worker statistics.
  - This should make it possible for users to predict how long a render job will be, as they can look up things like render times of similar render jobs.
  - The underlying design should be generic enough to store all kinds of statistics and events, not specific to Blender render times only.
  - This project would include the technical design of this feature, the frontend / UI / UX design, and the implementation of both front- and back-end.
  - Optional: record more statistics, such as per-frame render time, memory usage, etc.
  - Optional: show progress of jobs in the jobs list.
- *Expected outcomes*: A way for users to get a better understanding of how a new render job will behave, as they can look up information about past & currently running jobs.
- *Skills required*: Familiarity with Go and unit testing. Familiarity with web languages (HTML/CSS/JavaScript, VueJS).
- *Possible mentors*: [Sybren Stüvel](https://projects.blender.org/dr.sybren/).
- *Expected project size*: 350 hours
- *Difficulty*: hard

#### RNA Overrides

- *Description*: Render jobs should be able to specify "RNA overrides" ([#101569](https://projects.blender.org/studio/flamenco/issues/101569)).
  In other words, the job definition should be able to include some Python code that sets certain properties in Blender to certain values.
  - Design a way to include this in a job definition, and how it affects tasks & commands.
  - This could include the ability to add new RNA overrides to existing jobs.
  - This should include the ability to update those override values via the web interface.
  - Design how such additions / changes affect already-created tasks/commands in the database. Or a way to make this work without changing things in the database?
  - This can be used both when a user wants to change something (like re-rendering with increased sample count), or for Flamenco itself to adjust things in the blend file without having to save those values in the blend file itself ([#104264](https://projects.blender.org/studio/flamenco/issues/104264))
- *Expected outcomes*: Give users a simpler way to configure Flamenco for their needs.
- *Skills required*: Familiarity with Blender (for making the RNA overrides themselves work). Familiarity with Go and unit testing for adjusting Flamenco. Familiarity with web languages (HTML/CSS/JavaScript, VueJS) for the web frontend.
- *Possible mentors*: [Sybren Stüvel](https://projects.blender.org/dr.sybren/).
- *Expected project size*: 350 hours
- *Difficulty*: hard

#### Configuration Web Interface

- *Description*: Flamenco's configuration file can be created via its *Setup Assistant*, but that's only for the initial configuration. For managing more complex things, like the [two-way variables for cross-platform support](https://flamenco.blender.org/usage/variables/multi-platform/), users still have to manually edit the YAML file. This project is about introducing a configuration editor in the web frontend, and potentially new backend API functions to support that.
  - New tab in the frontend for managing the configuration.
  - A way to retrieve and visualise the configuration.
  - New front-end widgets to represent these, including more complex cases like one-way and two-way variables.
  - A way to save the edited configuration.
  - Optional: A validator for the configuration options, so that changes can be checked before they take hold.
  - Optional: A way for Flamenco Manager to load and apply the configuration without restarting the process.
  - Optional: A custom job type for validating configuration paths, so that, for example, a macOS path can be actually checked on a Worker running macOS.
- *Expected outcomes*: Give users a simpler way to configure Flamenco for their needs.
- *Skills required*: Familiarity with web languages (HTML/CSS/JavaScript, VueJS, OpenAPI). Potentially also familiarity with Go in case of backend work.
- *Possible mentors*: [Sybren Stüvel](https://projects.blender.org/dr.sybren/).
- *Expected project size*: 350 hours
- *Difficulty*: hard

#### Polish & Shine

- *Description*: Fix various issues & implement missing pieces to solve common bumps in the road.
  - Reconsider some design aspects of the web frontend, so that it works better on narrower / smaller screens.
  - Allow finishing setup assistant without Blender on Manager ([#100195](https://projects.blender.org/studio/flamenco/issues/100195)).
  - Introduce per-Worker logs on the Manager, for introspection and debugging.
  - Fix issues with two-way variables ([#104336](https://projects.blender.org/studio/flamenco/issues/104336), [#104293](https://projects.blender.org/studio/flamenco/issues/104293))
  - Add a web interface for the mass deletion of jobs. There is already an API call for this, which deletes all jobs older than a certain timestamp.
  - Other issues from the tracker.
- *Expected outcomes*: Improve the overall experience people have when working with Flamenco.
- *Skills required*: Familiarity with web languages (HTML/CSS/JavaScript, VueJS) for front-end work. Familiarity with Go and OpenAPI for backend work.
- *Possible mentors*: [Sybren Stüvel](https://projects.blender.org/dr.sybren/).
- *Expected project size*: 175 hours
- *Difficulty*: medium

### Geometry Nodes

#### Regression Testing

- *Description:* As people build more assets on top of Geometry Nodes, it becomes more and more
  important to ensure good backwards compatibility. This project focuses on improving our
  regression tests to cover more issues as early as possible. This involves:
  - Adding new tests in our existing test framework.
  - Extending the test framework to cover node tools, baking and maybe other areas we still have to find.
  - Preparing more complex production files for use in regression tests.
- *Expected outcomes:* Improved stability of Geometry Nodes.
- *Skills required:* Good in C/C++ and Python.
- *Possible mentors:* Jacques Lucke, Hans Goudey
- *Expected project size:* 90 or 175 hours depending on how many of the mentioned areas are covered
- *Difficulty:* Easy (using existing framework) and Medium (extending framework)

### Modeling

#### Improve Edit-Mesh Mirror

- *Description:*
  Blender's mesh mirroring in mesh edit-mode works for basic transformations,
  but does not work for most other operations such as sliding, smoothing, marking seams, etc.
  In practice, this makes the edit-mode mirror only useful in very specific circumstances and not
  for general modeling.

  While supporting every operation isn't practical, enabling it for a subset of operators such as
  those that only adjust existing geometry (rather than adding or removing geometry)
  would be immediately useful for artists.

- *Expected outcomes:* Improved edit-mesh mirror support for existing tools.
- *Skills required:* Proficient in C/C++.
- *Possible mentors:* [Campbell Barton](https://projects.blender.org/ideasman42)
- *Expected project size:* 175 or 350 hours
- *Difficulty:* medium

### Sculpt & Paint

#### Mesh Sculpting Performance Improvements

- *Description:* Last year's sculpting rewrite project gave a large improvement in performance, but the team didn't have the time to pursue everything.
  [This task](https://projects.blender.org/blender/blender/issues/130071) lists possible future improvements.
  This GSoC project would explore one or more of those ideas with in depth performance testing and experimentation.
- *Expected outcomes:* More interactive sculping with large meshes
- *Skills required:* Proficient in C++, familiarity with data-oriented-design.
- *Possible mentors:* [Hans Goudey](https://projects.blender.org/HooglyBoogly)
- *Expected project size:* 175 or 350 hours
- *Difficulty:* medium or hard

### VFX & Video

#### Hardware accelerated video encoding/decoding

- *Description*: Currently Blender encodes and decodes video though ffmpeg C libraries,
  on the CPU. ffmpeg also has support for hardware video processing (various kinds
  depending on platform), this project would enable usage of that.
  - Build ffmpeg with hardware video processing support included.
    - Note: Blender can't include "non-free" ffmpeg libraries (which means `cuda_nvcc`, `cuda_sdk`,
      `libnpp` can't be used).
  - On Blender's video decoding and encoding side, implement code that would use
    any relevant ffmpeg C libraries parts for hardware video processing, when supported.
  - Decide which additional UI settings need to be exposed to the user, to control
    hardware video processing.
  - Implement code needed to transfer video frames between hardware memory and CPU memory
    as needed (the rest of VSE processing pipeline is purely on CPU currently).
- *Expected outcomes*: Video encoding or decoding is more efficient by using dedicated hardware.
- *Skills required:* Proficient in C/C++, familiarity with ffmpeg.
- *Possible mentors:* Aras Pranckevicius
- *Expected project size:* 350 hours
- *Difficulty:* medium

#### High Dynamic Range (HDR) support for video

- *Description:* This project has several partially dependent parts that are all about
  HDR support within VSE:
  - Make Sequencer preview window be able to display HDR content on a capable display
    (like 3D viewport or Image window can).
  - Make blender movie reading code be able to decode HDR videos into proper scene-linear
    or sequencer color space as needed. HDR video data might be PQ or HLG encoded, and this
    might need special decoding into destination color space.
  - Make blender movie writing code be able to encode HDR videos. Blender already can encode
    10/12 bit videos, but only for regular LDR. Additional PQ or HLG data encoding and necessary
    video metadata is not currently done.
- *Expected outcomes*: HDR video handling is improved within Blender.
- *Skills required:* Proficient in C/C++, familiarity with ffmpeg, knowledge of color spaces and color science.
- *Possible mentors:* Aras Pranckevicius
- *Expected project size:* 350 hours
- *Difficulty:* medium

#### VSE: OpenTimelineIO support

- *Description:* built-in support for OpenTimelineIO import/export within Blender VSE.
  Blender Studio has [experimented with it](https://studio.blender.org/blog/opentimelineio-in-blender/) in 2021,
  by using and extending a 3rd party addon [vse_io](https://gitlab.com/superprod/stax_suite/vse_io).
  It might be useful to have built-in support for this.
- *Expected outcomes*: Blender VSE can import and export `.otio` files.
- *Skills required:* Proficient in C/C++, familiarity with video editing workflows.
- *Possible mentors:* Aras Pranckevicius
- *Expected project size:* 350 hours
- *Difficulty:* medium

#### VSE: Pitch correction for sound playback

- *Description:* Currently when audio is retimed, the pitch changes, would be nice to have an
  option to preserve pitch. Different approaches could be researched and implemented
  (e.g. pitch correction for mostly human speech might be different from pitch correction of music).
  Might need integration of some 3rd party library if it is suitable for the task, or implementing
  the correction algorithms manually.
- *Expected outcomes*: Retimed sound playback has options to preserve original pitch.
- *Skills required:* Proficient in C/C++, sound processing algorithms.
- *Possible mentors:* Aras Pranckevicius
- *Expected project size:* 350 hours
- *Difficulty:* medium

#### VSE: Animation retiming

- *Description:* Modify animation of strips, when changing their playback speed.
  Retiming allows changing playback speed of strips, but when strips are animated, the animation keys are fixed
  in position. These could be moved, such that animation is seemingly mapped to frames of the content.
- *Expected outcomes*: Animation proportionally is scaled with strip when retiming.
- *Skills required:* Proficient in C++.
- *Possible mentors:* Richard Antalik
- *Expected project size:* 175 hours
- *Difficulty:* medium

#### VSE: Keyframing in preview

- *Description:* Open workflow quick animation in VSE preview region.
  In 3D viewport it is possible to transform object and press I key to add key for its position.
  This also could be done in sequencer preview. This feature should follow same rules and preferences.
- *Expected outcomes*: Possibility of quick and easy animation in VSE preview
- *Skills required:* Proficient in C++.
- *Possible mentors:* Richard Antalik
- *Expected project size:* 90 hours
- *Difficulty:* medium

#### Compositor: Implement new nodes
- *Description:* The compositor has been rewritten to be more efficient and future proof.
  Moving forward, it would be nice to implement new nodes to make the compositor as powerful as it can be.
  Interested students are encouraged to propose their own ideas. Some example nodes include:
  - Define low / high points
- *Expected outcomes:* Implemented one or more nodes for both CPU and GPU backends.
- *Skills required:* Good in C/C++, image processing algorithms, familiarity with shaders.
- *Possible mentors:* Habib Gahbiche / Omar Emara
- *Expected project size:* 90 or 175 hours depending on the node
- *Difficulty:* Easy or Medium depending on the node

#### Compositor: UI improvements
- *Description:* The compositor has been rewritten to be more efficient and future proof.
  It would be nice to improve the UI of some nodes as well as the workflow overall.
  Interested students are encouraged to suggest ideas. Some examples include:
  - Implement 2D gizmos for exisiting nodes.
  - Re-design the UI of exisiting nodes
- *Expected outcomes:* Improved UI within node editors.
- *Skills required:* Proficient in C/C++, familiarity with design patterns.
- *Possible mentors:* Habib Gahbiche / Omar Emara
- *Expected project size:* 90 or 175 hours depending on the scope of the project
- *Difficulty:* Easy or Medium depending on the scope
