# New Developer Setup

## Build Blender

To start developing in the Blender source code, an important step is to [build
Blender](../building_blender/index.md). Follow the instructions to download the
source code and compile it into a Blender binary that you can run.

## Set Up a Development Environment

Any (programming oriented) text editor along with command line tools
can be used for Blender C/C++ and Python development.

However integrated development environments (IDE's) can make tasks
like searching the code, debugging and refactoring easier and more efficient.

Popular among Blender developers are:

- Cross platform: [Visual Studio Code](../development_environments/vscode.md) - a popular IDE,
  but takes some work to configure.
- Cross platform: [Qt Creator](../development_environments/qtcreator.md)
- macOS: [Xcode](../development_environments/xcode.md)
- Windows: [Visual Studio](../development_environments/visual_studio.md)


Some Blender developers use other IDE's or don't use an IDE at all.
With experience you may want use a development environment not mentioned here.
The list above is just suggesting some starting points known to work
for others.

## Basic Operations

If you're not familiar with your development tools yet,
we recommend at a minimum to learn to do these operations efficiently:

- Edit source code
- Lookup any file by name, for editing
- Search in text across the entire source code
- Rebuild after making changes

If you aren't sure how to do any of the operations above,
take some time to investigate how to perform them,
otherwise basic navigation and editing of the source will be unnecessarily difficult.

## Configuration

To write code using the correct style:

* For [C/C++](../guidelines/c_cpp.md),
  set up [clang format](../tooling/clangformat.md) in your editor.
  If that is not supported, configure it to use 2 spaces of indentation for C/C++ code.
* For [Python](../guidelines/python.md),
  configure your editor to use 4 spaces of indentation.
