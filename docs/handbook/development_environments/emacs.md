# Emacs

## ClangFormat

The [clang-format](https://melpa.org/#/clang-format) package
includes a minor mode to auto format on save called: `clang-format-on-save-mode`.

This is an example of how it can be used with `use-package`.

``` lisp
(use-package clang-format
  :commands (clang-format-buffer clang-format-on-save-mode))

(add-hook 'c-mode-hook 'clang-format-on-save-mode)
(add-hook 'c++-mode-hook 'clang-format-on-save-mode)
(add-hook 'glsl-mode-hook 'clang-format-on-save-mode)
```

See
[How to clang-format the current buffer on save?](https://emacs.stackexchange.com/questions/48500)
for other examples.

## RST

For editing reStructuredText for the user manual,
the following can be added to your `~/.emacs.d/init.el`.

``` lisp
(add-hook
  'rst-mode-hook
  (lambda ()
    (setq-local fill-column 120)
    (setq-local indent-tabs-mode nil)
    (setq-local tab-width 3)
    (setq-local evil-shift-width 3)

    (add-to-list 'write-file-functions 'delete-trailing-whitespace)

    ;; package: find-file-in-project
    (setq-local ffip-patterns '("*.rst" "*.py"))))
```
