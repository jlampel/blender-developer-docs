# Proxy system

To optimize video files playback performance, sequencer uses proxy
system which is available in movie clip editor too. Proxies are
re-encoded video files with settings to get good scrubbing performance
and they can have smaller resolution. Since proxy transcoding is
standalone system, its inner workings won't be described in detail here,
but rather its integration with sequencer. Same proxy system is used in
movie clip editor.

TODO:

- Image strips
- Generating editor side, sequencer side
- How proxies are used