# View

Views are containers for persistent per view data (e.g. Raytracing persistent buffers, Persistent culling data, etc...). The `render()` method of a view will call one or multiple pipelines to compute the view pixels. Then the result is outputed to a module data (e.g. `Film`, `SphereProbeModule` etc...).

## Main View

This view allows to render the output render or viewport. It is composed of at least one `ShadingView`. For panoramic rendering, it can use up to 6 `ShadingView` in order to cover the full spherical domain as a cubemap.

The `ShadingView` uses most of the modules and pipelines.

```mermaid
flowchart LR
  F(Background Pipeline) --> A(Deferred Pipeline) --> B(Forward Pipeline) --> C(Motion Blur) --> D(Depth Of Field) --> |Output| E(Film) 
```

## Capture View

A view to capture sphere probes. It can render either the world alone or the scene.

```mermaid
flowchart LR
  F(Background Pipeline) -->A(Deferred Pipeline) --> |Output| E(Cubemap Face Target) 
```

> There is a naming confusion error with this view. The `CaptureView` is used to render sphere probes but the `CapturePipeline` is used to turn the scene into surfels for volume probe. This is very confusing and should be cleaned up.

See [SphereProbe Module](modules/lightprobe_sphere.md) for more detailed informations.

## Lookdev View

This view is used to render the Lookdev HDRI Spheres. The spheres are rendered using an orthographic projection but with accurate positioning inside the scene. We render the spheres as if they were on the camera near plane with a relatively small radius.
Separating the lookdev sphere rendering from the *Main View* ensures that they will never be occluded and have no influence on the scene.

> At the time of writting, this view is *not* `LookdevView` but `LookdevModule::Sphere` and part of the `LookdevModule`. This is against design and needs some cleanup to move it to `eevee_view.cc`.

See [LookDev Module](modules/lookdev.md) for more detailed informations.
