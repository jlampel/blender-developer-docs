# Window

The **wmWindow** struct is the abstraction of a separate standalone
rectangular container of UI elements, usually framed by OS decoration
like a title bar, and participates with other application windows on the
user's desktop. Depending on the platform and use, it can close,
minimize, resize, roll-up, etc.

Specifically, wmWindow holds all the data needed to save and recreate
the window and all the data needed by Blender while it operates. This
includes the operating system-supplied handle, parent, scene, layer,
size, position, current cursor, event queue, handles, and more.

## Getting the Current Window

It is a simple call to get the one current Window, which is the one that
is active and in focus (currently receiving mouse and keyboard entry).
How a window becomes active can vary by platform. On some operating
systems you must click your mouse on the title bar or in the contents,
and on others your mouse hovering over it might make it active.

In most cases you need a pointer to the window for a user-interactive
purpose, so you can often avoid having to test the return for NULL. The
exception to be aware of is when Blender is run headless from the
command-line. You get the current window from the
[Context](../core/context.md) like this:

``` cpp
wmWindow *win = CTX_wm_window(C);
```

## Enumerating All Windows

The list of windows is kept by the [Window
Manager](window_manager.md). The following
loops over all the windows:

``` cpp
wmWindowManager *wm = CTX_wm_manager(C);
LISTBASE_FOREACH (wmWindow *, win, &wm->windows) {
}
```

## Temp Windows

It is the intention that the positions and sizes of windows are saved
with the blend and restored when you load. However, we consider some
windows to be "temporary". These include Preferences, File Manager, and
Render Window, and these will not be restored when you load the file.
You can find this status by calling `WM_window_is_temp_screen(win)`

## Window Parenting

Window behavior should vary between platforms to provide expected
behavior. Generally though our windows are parented to the window that
created them. You can create a window that is top-level like the main
window by using the `toplevel` argument of `WM_window_open`. For
users, selecting `Window / New Window` will create a child, while
`Windows / New Main Window` will create a top-level window that can
create its own children. Having the two options allows many different
ways of working with multiple windows.

## Creating a New Window

The following will open a new window containing a 3D Viewport
[editor](editors.md):

``` cpp
wmWindow *win = WM_window_open(
    C, "Title", 0, 0, 800, 600, SPACE_VIEW3D, false, false, false, WIN_ALIGN_ABSOLUTE);
```

## Typical Window Operations

Once you have a pointer to a window, following are just some typical
ways of operating on it:

``` cpp
wm_window_lower;
wm_window_raise;
wm_window_set_size
wm_window_close
wm_window_free
```

## API

For more specific information, look at these files:

``` cpp
source/blender/makesdna/DNA_windowmanager_types.h
source/blender/windowmanager/intern/wm_window.cc
```
