# Monado

Monado is an open source XR runtime and an implementation of the OpenXR API for Linux. It is
particularly useful for Blender development to simulate an XR headset if the developer doesn't have
one.

## Installation

The [installation guide](https://monado.freedesktop.org/getting-started.html#monado-installation)
provides instruction for installing and building Monado from source. It is not widely packaged, so
you will likely have to build it from source, but building it is straightforward.

If you only intend to use it for simulation, then you don't need most dependencies, and only a [few
required dependencies](https://gitlab.freedesktop.org/monado/monado#getting-started) are needed.

## Configure

Since multiple XR runtimes might exist, we need to point the OpenXR loader to the Monado runtime.
The OpenXR loader looks at the `active_runtime.json` file in the [active runtime
location](https://registry.khronos.org/OpenXR/specs/1.0/loader.html#linux-active-runtime-location)
to know which runtime to use. If your distribution package added that file, then you don't need to
do anything.

If your distribution package didn't provide the `active_runtime.json` file, it might have provided
an `openxr_monado.json` file, which you can symlink to the active runtime location. But if that
file was also not provided, then the file should simply be:

```
{
    "file_format_version": "1.0.0",
    "runtime": {
        "name": "Monado",
        "library_path": "/path/to/libopenxr_monado.so",
        "MND_libmonado_path": "/path/to/libmonado.so"
    }
}
```

If you built Monado from source, an `openxr_monado-dev.json` file will be in your build directory,
which you can use instead. Alternatively, you might want to simply point the `XR_RUNTIME_JSON`
environment variable to your JSON file instead of symlinking it.

## Use

First open the Monado service by calling the `monado-service` binary or by starting the Systemd
unit provided by your distribution package. If you do not have a headset, the Simulated driver will
be used, which is what we want.

Then open Blender normally if you had setup an `active_runtime.json` file, otherwise open Blender
with the environment variable:

```
XR_RUNTIME_JSON=/path/to/openxr_monado-dev.json blender
```

Once you start a VR session, a window will open that includes the simulated XR output. Once done,
close the Monado service by pressing return.
