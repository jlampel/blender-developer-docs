# Shader C++ compilation

This tool allow for a C++ compiler to check the shader code.
This has multiple benefit:

- Ensures to catch some type casting errors that are not enforced by some shader compilers.
- Ensures compliance with our MSL translation layer.
- Allow IDE integration for easier navigability.
- Allow clang-tidy and other C++ tooling.

Most of the GLSL syntax is covered by `gpu_glsl_cpp_stubs.hh` which contains most of the code that allows such compilation.
If some valid overload or function is missing, please check with the module owners before adding it.
It might be that we do not support it on purpose for compatibility reasons.

## Enabling

Compiling blender with `WITH_GPU_SHADER_CPP_COMPILATION=On` will create one executable for each of the shaders.
Note that enabling this increases compilation time and should better be turned off when bisecting.

It is worth noting that these executables are not working implementation of the GPU shaders to use for rendering on the CPU.

The file association needs to be changed in the IDE to associate `.glsl` files to C++.

It is possible that the [`compile_commands.json`](../../../handbook/development_environments/vscode.md#compile-commands)
needs to be linked to the IDE for some features to work.

## Adding support for a shader

Each shader folder should have a dedicated `CMakeList.txt` that contains the compile command for each of the shader file.
Refer to existing cmake files to see how to do that.

In the shader file, the appropriate resources needs to declare by using one of these macros:

- `VERTEX_SHADER_CREATE_INFO(create_info_name)`
- `FRAGMENT_SHADER_CREATE_INFO(create_info_name)`
- `COMPUTE_SHADER_CREATE_INFO(create_info_name)`
- `SHADER_LIBRARY_CREATE_INFO(create_info_name)`
Note that multiple of these can be added to a single file.

This will also make sure to declare any globals like `gl_LocalInvocationID` of `gl_VertexID` depending on the shader type.

The create infos header files containing the needed infos needs to be included before the `*_CREATE_INFO(create_info_name)` macro.
Note that theses includes will be bypassed by the GPU shader compiler.

If a shader file ins't linked to a particular create info, it can directly include `gpu_glsl_cpp_stubs.hh` which will also be bypassed by the GPU shader compiler.

### Common issues
- Make sure the create info actually references the shader file in some way (through its shader sources or their dependencies).
- Make sure the create info header also include the create info header that contains the additional create infos.
- Make sure the `CMakeList.txt` includes all required paths for includes to work.
- Some shader libraries cannot be compiled as standalone with this feature because they rely on conflicting shader stage globals.