# Device Management

## Device selection

A system can have multiple GPUs. Blender uses a single GPU for
Vulkan. Device selection is done by scoring different devices
based on the type of GPU (software, integrated GPU, descrete GPU)
, the availability of extensions and an user preference override.

## Extensions & features

`vk_backend.cc::missing_capabilities_get` is used to check for minimum
requirements. Optional extensions and features are initialized in `GHOST_ContextVK.cc`. They need to be kept in sync.

### Required
In order to run Blender the next features and extensions are required:

- Vulkan 1.2 core
    - geometry shaders
    - logical operations
    - dual source blending
    - image cube array
    - multi draw indirect
    - multi viewport
    - shader clip distance
    - draw indirect first instance
    - fragment store and atomics
- Extensions
    - `VK_EXT_provoking_vertex_extensions`

### Optional
The next features are optional and will be used when available
- sampler anisotropy
- shader draw parameters
- shader output layer
- shader output viewport index

The next extensions are optional and will be used when available
- `VK_KHR_dynamic_rendering`
- `VK_EXT_dynamic_rendering_unused_attachments`
- `VK_EXT_dynamic_rendering_local_read`
- `VK_EXT_shader_stencil_export`
- `VK_EXT_swapchain_colorspace`
- `VK_KHR_maintenance4`
- `VK_KHR_fragment_shader_barycentric`

> NOTE: Dynamic rendering local read will only be activated on Qualcomm devices. The implementation has performance regressions when running on non-tiled GPU architectures.
> It is possible to test dynamic rendering local read using the `--debug-gpu-vulkan-local-read`.

## Workarounds

Optional features/extensions or capabilities are ignored or have a workaround to simulate.

The enabled workarounds are stored in `VKWorkarounds`.

> NOTE: VKWorkarounds will be refactored to remove the double negative. The struct will be spliced into what is a workaround (missing capabilities) and what is supported.

## Block list

There are cases where drivers are known to be not working with Blender. These drivers
will be ignored. The block list is implemented at `blender::gpu::GPU_vulkan_is_supported_driver`.