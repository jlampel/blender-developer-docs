# Breaking up the Modifier Stack

This document does not propose any concrete solutions. Instead it shows,
that the existing set of modifiers mixes multiple conceptually different
systems. Therefore, it is unlikely that all modifiers will end up in the
same system in the future.

Separating these concepts has multiple benefits:

- The UI can be optimized separately for different kinds of modifiers.
- Implementing a new "modifier" becomes easier, because the API is
  designed for the specific concept.
- Fewer special cases have to be considered.
- The strengths of individual concepts can be embraced better (e.g. by
  implementing a node based mesh manipulation system or a more generic
  simulation system).

As I see it, the existing modifiers belong to four categories. The
specific names are somewhat arbitrary.

### Function

These modifiers just take a mesh and other data as input, and create a
new mesh. Most existing modifiers are in this category.

Examples: Array, Decimate, Subdivision Surface, Remesh, Warp, Ocean,
Data Transfer.

Turning these modifiers into nodes, that are part of a data flow graph,
is comparatively straight forward. They can easily be grouped and reused
on different objects.

### With Binding

Modifiers in this category are bound to some original object. These
modifiers cannot be turned into nodes in a data flow graph that easily,
nor should they. Generally, it does not make sense to use more than one
of these modifiers on a single object. Furthermore, they usually are the
first modifier.

Examples: Laplacian Deform, Mesh Deform, Multiresolution.

### Simulation

The output of these modifiers depends on the previous frame, making them
history dependent. All modifiers in the existing Simulate category
belong here.

Many simulations handle interactions between multiple objects.
Therefore, having these modifiers on a per object bases, and mixing them
with unrelated modifiers, seems wrong.

### Final Output

The only modifier that partially fits into this category is the
subdivision surface modifier. With OpenSubDiv we can benefit from GPU
accelerated generation of the subdivided mesh. This only works, when the
modifier is the last one in the modifier stack (afaik). In my opinion,
rules like that should be avoided and this option should become
independent of the modifier. Also this functionality can't really be
used with nodes.
