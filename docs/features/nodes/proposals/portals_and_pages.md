# Portals and Pages Prototype

## Pitch

The purpose of this prototype is to test the concept of "portals" and
"pages" to better organize the nodes within a single node group. A node
group is split up into pages. Nodes on the same page are supposed to
belong more together than nodes on different pages. Data can be passed
between pages using portals.

## Prototype

The prototype allows users to insert a portal. A portal consists of two
nodes that are internally connected through an id. The data that is
passed into the "Portal In" node comes out of the "Portal Out" node. A
portal also has a name. Changing the name on one of the two nodes,
changes it on both. Pressing tab while a portal node is active, moves
the view to the other corresponding portal node.

![](../../../images/Insert_portals.png)

The prototype exposes five pages. One can switch between the pages in
the header of the node editor. Internally, a page is just a large square
within the node group. Switching between pages is therefore the same as
moving the view horizontally far enough. Selected nodes can be moved to
another page by clicking on the page number while holding down ctrl.

![](../../../images/Pages_switch.png)

An example file and more screenshots can be found in
[T86583](https://developer.blender.org/T86583).

## Findings

- The concept of a page node can be very similar to of the local node
  group. The difference is that the page exists in a more permanent
  place that is always accessible in the UI/via shortcuts.
- Having portals as the main way to go in and out of pages adds a lot of
  overhead since users would need 4 portals per page in average. An
  alternative is to have explicit page inputs and outputs, and a "Page
  node" to add access to it from another part of the nodetree.
- Portals need to be visually distinguishable from other nodes for this
  to work. They can be useful even without the pages concept, but they
  are fundamental to be able to connect different pages among
  themselves.
