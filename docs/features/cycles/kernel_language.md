# Cycles Kernel Language

## Overview

The kernel contains the core rendering routines, where most of the
render time is spent. The code here can be compiled as a subset of
C++14, CUDA and HIP. In order to support this, we must be careful to
only use language features that are supported for all targets, with a
few preprocessor macros to smooth over the language differences.

In short, this currently means:

- C++14 syntax
- Vector types like float3 or uchar4, with common operators
- Constant memory for small amount of fixed parameters
- Global memory for most read-only data
- Texture handles for GPU devices that natively support image textures
- No call stack, no recursive functions, function pointers or virtual
  functions
- No dynamic memory allocation
- No doubles, only floats and half floats

## Vector Types

The vector types are the same as CUDA: float2, float3, float4, and
similar for int, uint, uchar. Common operators like add, multiply, etc
work as expected. For construction, use the `make_*()` functions, for
example:

``` c
float3 v = make_float3(0.0f, 1.0f, 0.0f);
```

We define the necessary classes and operator overloading to implement
them for all supported platforms

## Qualifiers

- `ccl_device`: for functions, all kernel functions should use this
- `ccl_global`: for pointers to global memory (mostly function
  parameters)
- `ccl_local`: explicitly places stack variables in local memory
- `ccl_shared`: shared memory
- `ccl_constant`: constant memory

These are defined as macros for each target. Some targets may define
some qualifiers as empty, or may accept them only in particular
contexts, so it's best to test compiling all. `ccl` can be considered
an abbreviation for `cycles` or `cycles compute language`, whichever
you prefer.

## Constant Memory and Textures

Small, fixed size data is stored in constant memory. KernelData
kernel_data contains all constant memory, and is available as a global
variable everywhere.

All large read-only data is stored in a small number of arrays in global
memory, and texture handles for image textures. These global memory
arrays are accessible as global variables in the kernel, and must be
accessed through the `kernel_tex_*` functions.

For historical reasons this is still called "textures", as old GPU
architectures had to use texture memory for good performance. This will
be renamed at some point.
