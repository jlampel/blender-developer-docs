# Guidelines

Process and recommendations to help with extension submit approval review.
This can also guide extension authors to make sure their submissions are
up to the highest standards.

## General

1. Make sure the [Terms of Service](https://extensions.blender.org/terms-of-service/) are respected.
   ([example](https://extensions.blender.org/approval-queue/cadbase-library/))

0. Check if it works in Blender (run some basic functionality)
   * Most often the add-on is just not working, since it was poorly converted into an extension
     without testing ([example](https://extensions.blender.org/approval-queue/nd/#activity-159)).
   * Sometimes the add-on was never updated for 4.2 specific API
     ([example](https://extensions.blender.org/approval-queue/dragengine-tools/#activity-151)).<br>
     Refer to the breaking API changes:
     [4.0](https://developer.blender.org/docs/release_notes/4.0/python_api/#breaking-changes) /
     [4.1](https://developer.blender.org/docs/release_notes/4.1/python_api/#breaking-changes).

0. Check the manifest, common problems:
   * Permissions are not correct (`files` is expected for most I/O add-ons).
   * Optional arguments left in with default values (it shouldn't happen anymore since the guide was updated)

0. Python: Make sure there is no auto-updater on the code
   ([example](https://extensions.blender.org/approval-queue/textify/#activity-126)).

0. Python: pip dependencies should be
   [bundled as wheels or vendorized](https://docs.blender.org/manual/en/dev/advanced/extensions/addons.html#bundle-dependencies).

0. Python: If using `network` permission, the add-on is expected to check for `bpy.app.online_access`.

0. Interface: When possible, it is nice to share some advice on following UI's guidelines
   ([example](https://extensions.blender.org/approval-queue/y-projectionnode-form-camera-f1/#activity-165)).

0. Check the overall quality of the images.

0. Make sure there are no heavy image (`.gif` images in particular)
   on the image description, making the page massive to load.

0. Make sure the author is the owner/maintainer of the extension.

0. Make sure the add-on is self-contained.
   * This is the most delicate one and if in doubt ask on the
     [#extension-moderators](https://chat.blender.org/#/room/#extensions-moderators:blender.org) room.
   * It should not have functionality which is extended by external software.
   * It should not depend on external servers (`localhost` is fine).

## Violations in Code

1. Check all uses of `__file__`, this relates to two common mis-uses of this:
   * Manually constructing paths to add to `sys.path` and import modules into
     the global module name-space.
     Instead, sub-modules or wheels must be used.
   * Constructing paths to write to, when it must not be assumed the add-on directory is writable.
     Instead `bpy.utils.extension_path_user` must be used.

0. Check all uses of `open`, especially writing files
   to ensure the script is writing to a valid location.

0. Check all uses of `__name__`, for legacy add-ons this matches the add-on ID.
   For extensions `__package__` is used for the add-on identifier,
   e.g. `context.preferences.addons[__package__]` will return the add-on.
   The following expression is a common mistake: `context.preferences.addons[__name__].preference`

0. Check for all uses of the `sys` module.
   In general it should only be considered read-only.
   Direct manipulation or assignment of any of it's values is a hint that the extension
   may be making global changes that impact other modules.

0. Check there are no hard coded repository module names `user_repository` or `blender_org`.<br>
   Also check the literal `bl_ext` is never used as this is practically always a bad sign that an
   extension is either manipulating another extensions or not using relative imports when it should.

0. Check access to `bpy.context.window_manager.keyconfigs.addon`
   accounts for None (in background mode).

0. Check online access.
   * Search for imports: `requests` & `urllib`.
   * Search for `http` literal.
   * Double check all access first checks `bpy.app.online_access`
     and sending any data to a 3rd party is an explicit user action.

## Code Quality

Some extensions have low quality code, while not violating any rules,
it's likely to cause problems and can be noted in review.

1. Check for back-slash characters (`\`) use as path separators
   (that aren't used for escaping or line continuations).<br>
   These wont work on Linux or macOS.

0. Check direct access to Blender data: `bpy.data.[a-z_]+\.`
   This often hints at weak logic, for example:
   * `bpy.data.scenes[bpy.context.scene.name]`
      This can be a bug as two data-blocks may have the same name when one is from a library.
   * `bpy.data.worlds["World"]`
     Hard coded data-block names may not exist.
     In general it's a bad sign if scripts are assuming data-blocks with special names exist.

0. Check access of `bpy.data.objects`.
   This is rarely useful, in most cases the current scenes objects should be accessed instead.

0. Incorrect Quoting (Python expression)

   When generating Python expressions...

   `file.write('open("' + path + '")'` should be:<br>
   `file.write('open(' + repr(path) + ')'`

   _Or use the `repr` formatting specifiers for `str.format` or percentage-formatting:
   `{!r}` or `%r` respectively._

0. Incorrect Quoting (Blender's Data-Paths)

   `insert_keyframe('"pose.bones["' + bone.name + '"].property')` where `bone.name` should be<br>
   `bpy.utils.escape_identifier(bone.name)`

0. Incorrect Quoting (Command Line Arguments)

   `os.system('some_command --path "' + filepath + '"')` should be:<br>
   `os.system(shlex.join(["some_command", "--path", filepath])`

0. Run a linter on all scripts:
   `find "." -iname "*.py" | xargs ruff check --output-format=concise --ignore=E402,E741,E731`

   _While this doesn't need to pass without warnings: large numbers of unused imports,
   undefined variables or invalid syntax are a bad sign and might hint at inclusion of
   outdated/unused code._
