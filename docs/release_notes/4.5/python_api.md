# Blender 4.5 LTS: Python API

## Additions

- `KeyConfig.keymaps.find_match(keymap)`

  Add a convenience function to find the matching key-map from another key-configuration.

  This avoids having to pass multiple arguments to the existing `find` method.

  (blender/blender@28126b83a527c081677d45906e2d18dc1c0bf976).


- `KeyMap.keymap_items.find_match(keymap, keymap_item)`

  Add a method to support looking up key-map items from other key-maps.

  This allows an add-ons preferences to show the key-map editor for key-map items
  they define while maintaining the original key-map entries (used by "Reset to Default").

  Typically the `find_match` method should be called on key-maps from:
  `context.window_manager.keyconfigs.user`
  to lookup key-map items which the add-on created in:
  `context.window_manager.keyconfigs.addon`.

  (blender/blender@4a6d687d53b42a6ce69ea324f59ee7202777056b).
