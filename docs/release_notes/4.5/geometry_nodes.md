# Blender 4.5 LTS: Geometry Nodes

### Performance
* Vertex to edge attribute domain interpolation is roughly 1.7x faster (blender/blender@33db2d372fc5).

### Misc
* New *Visual Geometry to Objects* operator (blender/blender@63e6f53ba06750).