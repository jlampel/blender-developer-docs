# Blender 4.5 LTS: Core

## Big Endian Support Deprecation

Blendfiles saved by Blender from a Big Endian platform will trigger a warning when opened. See [that annoucement](https://devtalk.blender.org/t/big-endian-support-deprecation-removal/39098) for details.
