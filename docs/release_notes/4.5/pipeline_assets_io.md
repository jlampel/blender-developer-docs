# Blender 4.5 LTS: Pipeline, Assets & I/O

## USD
- Support additional USD Shape primitives: Plane, Capsule_1, and Cylinder_1
  (blender/blender@bbab2aa32843877a964851ea7f8cf48416096cd6, blender/blender@92aab7c3db2dfb4fa136c7aeaa35c8ea6fde5157)

## Other
- Support importing multiple SVG files (blender/blender!134795)