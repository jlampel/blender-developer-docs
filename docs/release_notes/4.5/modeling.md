# Blender 4.5 LTS: Modeling & UV

* The "Separate" and "Join" operators are implemented for the new curves type (blender/blender@a99f9496a082, blender/blender@5a6d2e1ce29b).
* The "Split" operator is implemented for the new curves type (blender/blender@2c4229455747).