# Blender 4.3: Pipeline, Assets & I/O

## USD
- Point cloud export is now supported
  (blender/blender@460aa3c231321ebfc8208ab65f18826066f72c60)
- Material purposes are now selectable during import
  (blender/blender@80cf6b99c3820f8179516b95733031aec2183a64)
- Improved handling of `@asset@` paths inside USD files
  (blender/blender@8a97f31e7694908fadee3db2305488250bbd63cf)
- More efficient export of animated attribute data when unchanged from prior frames
  (blender/blender@3c394d39f2ced658d990f437981f1d855917b1cb)

## glTF

- Importer
  - New features
    - Enable Draco mesh compression for bpy as a module (blender/blender@b25da97fefdf8854ffda3884dce16a2c92f9b69e)
  - Fixes
    - Fix importing Vertex Color on point / edges (blender/blender@98a7aa2c41c03ebf7f29915a98c3bf9161a5fc47)
- Exporter
  - New features
    - Set UDIM material names with tile number (blender/blender@58141b739607d01c1cda835eb9bf51249ec2000b)
    - Manage Quaternion & Matrix attribute types for custom attributes (blender/blender@51222557397cc55556650fe670c07ec0ecaf3e6d)
    - Enable exporting joint leaf at tail of leaf bones (blender/blender@8d575d3c8540499b9bbd52525572aaaa6685064d)
    - Logging
      - Add log level control (blender/blender@11451a208228effc12ee7ad7b90a79e616d85874)
      - Split stdout & stderr logging (blender/blender@1d108b0d77dfff984770cb82b2d5859d44100d84)
    - Hook UI: distinct import & export draw code (blender/blender@89c389c465b6bc6046c8408627fde7c186639f08)
  - Fixes
    - Avoid crash when animated + full collection hierarchy (blender/blender@3d0db481f49443b212ae205d4384bd9ce423d8b2)
    - Fix UDIM export crash when not rectangular tile locations (blender/blender@0e881a777253807a24e1cabcd78eccf8d49ef6fa)
    - Fix export UDIM when some tiles are empty & jpg/webp check (blender/blender@1f5d24fe0d084f7a9e0574bf24bf687ec64ecacf)
    - Fix UI after Blender changes (blender/blender@42840ded01948b9820389313551cb68c3f77e0ad)
    - Fix custom properties export (blender/blender@6191eb1e9df0637b22db805de6b97db40321b5e7)
    - Avoid double export (check instances inside instances collection with GN) (blender/blender@7c6975f6c88ffffda4e9514265ae4ebf9ddd5e27)
    - Fix regression RGB to Shader socket for unlit (blender/blender@69ab986370d989ff0300ecfca4eefa2eb93eb7f5)
    - Use wait cursor insead of empty progress cursor (blender/blender@4dad2a567d781e86d24b164a2f065d871c5957fe)
    - Children type check refactoring (blender/blender@c132bd896fa34e1dbbf39f66212c11d235dccde0)
    - Fix error message if hook failed (blender/blender@780721de1949256d367448e474ca963890e0d473)
    - Remove a duplicate lookup (blender/blender@936ca7636a54b9830b8770e8ac3d7271d14fdd1e)
    - Fix accessor min/max float check (blender/blender@4c441c1c6447323b59611b0ce35bb47e3535abfe)
    - Fix light spot default value check (blender/blender@044d20fe469e0897c6efe4b2e9f73252bcf7f809)
    - Account for library when gathering image URI (blender/blender@0169abd220b56efcf3d9d280be8751ffd9cf5fce)
    - Fix missing hok parameters leading to crash (blender/blender@a9bfca5483dced10e685513811a9db535bf28967)
    - Fix crash using full colleciton hierarchy export (blender/blender@d5f1ff250b7d9a109a29b89b7fe00e2ab7f9573c)
    - Fix exporting Vertex Color on point / edges (blender/blender@0208582c16c425ad706612a5a29800515866d42c)
    - Fix crash managing shader node groups when multiple links on same socket (blender/blender@3cfeebe7c74635f081c068462f03fb1f9129c0d4)
    - Fix regression exporting shapekeys when the only modifier is Armature (blender/blender@080877be6bc44e882ce0ddef336b3824cfdf854f)
    - Fix alpha when performing RGB2BW conversion (blender/blender@16754328f64f77a9289ec027bfbb38621f805e3b)
    - Fix typo in hook name (blender/blender@fb2edb17d9779d5e01aaa047dd30a7ba816218ce)
    - Make KHR_materials_variants a not required extension (blender/blender@0b3010d84cc3e3e02517b79abe973596b94a804e)
    - Remove some typing leading to crash (blender/blender@2c67b22bbbf3a6cb8a719b955e5aa7704bd25c92)
    - Fix regression exporting emission with texture (blender/blender@d723afe49f832b495cc4a879e9d8ba81d430aa52)
    - Fix regression exporting animated camera (blender/blender@c872ae495933b782008a99af60425ff42b46836b)
    - Add some poll sanity checks (blender/blender@b6346312c4751566a456d295e10009189abd3881)
    - Fix crash when sampling step is not 1 and actions are not aligned (blender/blender@912c85bb9d9131d34941688c08d84804a0e47c31)

