# Blender 4.3: Grease Pencil

Grease Pencil was rewritten to remove deeper limitations and improve overall performance.

## New Features
While the focus of the rewrite was compatibility and feature parity with Blender 4.2,
there are some new features that come with this release.

### Layer Groups

Layer Groups are a new way to group layers together.
They allow for easy toggling of visibility, locking, and onion skinning for all the
layers in the group. They can also be color-coded using color tags.
See the manual for more information.

![Grease Pencil Layer Tree](images/gp-layer-tree.png)

### Brush Assets

Just like other areas of Blender, Grease Pencil now uses the brush asset system.
See the release notes about [Brushes](sculpt.md#new-brush-management-workflow)
for more details.
Note that to [add a brush to the brush asset library](sculpt.md#converting-brushes),
the brush needs to be marked as an asset and stored in a `.blend` file in the
user-defined asset library.

![Brush Assets in the Asset Shelf](images/gp-brush-assets.png)

### Draw Tool Changes
The behavior of the draw tool has been updated.

* **Radius Unit**:
  In Blender 4.2, the Grease Pencil brushes used "pixels" to define the size of the brush.
  This unit was misleading because default the `"Strokes Thickness"` setting was
  `"World Space"`, meaning the size of the brush was actually converted to
  blender units (e.g. `"Meters"` by default).
  Now in Blender 4.3, users can set the **Radius Unit** of the brush to `View` or `Scene`.
  Using `View`, the brush size is in pixels relative to the current view.
  Using `Scene` the size of the brush is defined by the `Length` units of the scene.
  By default, the option is in meters.
  
* **Spacing**:
  In Blender 4.2, this was called `Input Samples` and was used to generate more points between
  samples. This setting is now called **Spacing**. It's defined as a percentage of the current brush
  size. For example, when the spacing is set to 100%, the draw tool ensures that every point is at
  *at most* one brush size away from the next point. When the setting is at 50%, it ensures that
  every point is at most 50% (or one half) of the brush size away from the next point, etc.
  
  **Important**: When the spacing is set very low (e.g. < 50%), the performance of the brush tool might
  degrade. It's recommended to keep this setting at 100% unless it's absolutely necessary to generate
  more points.

* **Active Smoothing**:
  The active smoothing algorithm has been rewritten. It's now based on an iterative
  curve-fitting approach resulting in a less "floating" more accurate feel.
  The `Active Smoothing` setting controls how tight/close the curve fitting
  is to the original samples. Higher values result in smoother but less accurate lines.

  ![Draw Tool Advanced Panel](images/gp-draw-tool-advanced-panel.png)

* **Screen Space Simplify**:
  In Blender 4.2, the `Simplify` setting of the draw tool was measured in real world distance.
  This meant that it would behave very differently at different scales/zoom levels.
  To solve this, the setting is now measured in pixels.
  Higher values result in more points getting removed.

  ![Simplify Setting in Pixels](images/gp-simplify-px.png)

* In Blender 4.2, the draw tool used a temporary stroke buffer, sometimes causing the
  stroke to "jump" (e.g. behind meshes) after releasing the pen. In 4.3, the draw tool now draws
  directly to the drawing in the right location.

### Eraser Tool Changes

The eraser has been rewritten to allow strokes to be "cut". Instead of just deleting points,
it now correctly solves for the intersections of
the eraser's edge and creates new points on the stroke. The `Soft` eraser also does this but
for multiple levels of transparency radiating outwards from the center of the eraser.

### Geometry Nodes

Grease Pencil is now supported within Geometry Nodes. 
See the [Geometry Nodes](geometry_nodes.md#grease-pencil) section on the release notes for more information.

### Fill Gradient tool

A new tool to edit the fill gradient was added.
It can be found in Edit Mode in the tool bar below the Interpolation tool.
Users can change the fill gradient (defined in the material settings) by selecting the fill
strokes and then click-dragging with the Gradient tool active.
(blender/blender@1b6220a4baf551dabc383df45f564da3208093a8)

![Fill Gradient Tool](images/gp-fill-gradient.png)

## Performance Improvements

Grease Pencil now uses multiple threads whenever possible, across the board.
Depending on the hardware and the number of CPU cores available, many operations within
Grease Pencil such as modifier execution are expected to be as fast or faster.

## File Size Improvements

Grease Pencil data is now much more compact. This can lead to a substantial decrease in file size.
This decrease also leads to faster load times and can also increase the speed of undo.

Here is a comparison of the size on disk of some test files (saved with the `Compress` option):

| Test File                                                                                 | Blender 4.2 | Blender 4.3 | Change        |
| ----------------------------------------------------------------------------------------- | ----------- | ----------- | ------------- |
| [Pirate by Sergi Miranda](https://cloud.blender.org/p/gallery/5edd13a01e5ba50266481719)   | 158.8 MB    | 64.4 MB     | ~2.5x smaller |
| [Picknick by Susanne Weise](https://cloud.blender.org/p/gallery/6122de81189bfc6a809b0ddc) | 147.3 MB    | 42.8 MB     | ~3.4x smaller |

## Miscellaneous Changes

* The tools in draw mode have been reordered: The primitive tools are now
  grouped together and part of the drawing tools in the first section.
  ![Draw Mode Tools](images/gp-draw-tools.png)
* The trim tool can now do multiple trims on the same stroke at the same time.
* The Layer selection popover in the top right corner of the 3D view has been moved to the top-bar.
  ![Draw Mode Topbar](images/gp-draw-topbar.png)
* The `Canvas` option has been moved out of the object-data properties. It's now part of the overlays in the 3D viewport.
* Grease Pencil objects can be parented to armatures using the `Envelope Weights` option.

## Conversion & Compatibility
From 4.3 and onward, Grease Pencil objects are automatically converted to the new architecture on
file load.
**There is no forward compatibility; Grease Pencil files created or saved in 4.3 or higher
will not load correctly in 4.2 or lower.**

Files created before Blender 4.3 should look almost identical when opened in 4.3.
Some differences include:

* The "Noise" modifier might give different noise results.
* If multiple "Time Offset" modifiers are used, results can be different because their order
  in the stack (relative to other "Time Offset" modifiers) behaves differently than in 4.2. 
  For more details see [this comment](https://projects.blender.org/blender/blender/issues/126977#issuecomment-1290997).

### Layer Adjustments

In Blender 4.3 Layer Adjustments were applied *before rendering*.
In 4.3 they are now applied *before modifier evaluation*.
This means that modifiers can make use of the changes made by layer adjustments and
evaluated geometry can account for these changes as well.

To make sure files created before 4.3 look the same,
the layer adjustments are converted into modifiers (last in the stack)
and then reset to their default value.

### Thickness Scale

In Blender 4.2 Grease Pencil data had a "Thickness Scale" setting
(called `pixel_factor` in python). This setting no longer exists in 4.3.

To make sure files created before 4.3 look the same,
the thickness scale is converted to a modifier at the very end of the stack.

## Deprecated Features
Some features present in 4.2 have been removed.
This is mostly due to the changes in the architecture and the difficulty of maintenance. 

### Screen Space Stroke Thickness
Strokes are now always in "World Space" thickness mode. While the "Screen Space" option is no longer availible.
The same effect can be recreated using a geometry nodes modifier. 
See the **[migration process](grease_pencil_migration.md/#screen-space-stroke-thickness)** for more information.

### Interpolation Tool Selection Order
The selection order (in edit mode) that was used to specify pairs of strokes to interpolate, has
been removed.
Porting this feature was not trivial, so it was decided to replace it with a better version in the
future.

### Drawing Guides
The guides in Draw Mode have not been ported to 4.3. There were multiple standing issues that made
it difficult to convert the code without introducing more bugs. It was decided that this will be
replaced with a more capable system in a future release.

### Curve Edit Mode
In edit mode, it was possible to toggle "curve editing" which would convert selected strokes
into bézier curves. This feature has been replaced with the `Curve Type` of strokes.
In edit mode, the curve type of strokes can be changed using the `Set Curve Type` operator.
See the manual for more details.

## Python API Changes

The Python API for Grease Pencil was rewritten to accommodate for the new data structure.
For add-on developers wanting to migrate their add-ons to 4.3,
see **[python migration process](grease_pencil_migration.md/#python-api)** for more details.
