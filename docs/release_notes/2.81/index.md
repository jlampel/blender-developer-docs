# Blender 2.81 Release Notes

Blender 2.81 was released on November 21, 2019.

Check out the final [Release Notes on
blender.org](https://www.blender.org/download/releases/2-81/)

  

## [2.81a Corrective Release](a.md)

This corrective release does not include new features and only fixes a
few serious regressions introduced in 2.81 release.

## [Cycles](cycles.md)

![](../../images/Cycles_displacement_cracks.jpg){style="width:400px;"}

New shaders for texturing, denoising with OpenImageDenoise, and NVIDIA
RTX support.

  

## [Eevee](eevee.md)

![](../../images/Eevee2.81_transparent_bsdf1.png){style="width:400px;"}

Shadows, transparency and bump mapping redesigned for easier setup and
better quality.

  

## [Viewport](viewport.md)

![](../../images/Demonstrating_Multi-layered_Matcaps_(Image_from_Pablo_Dobarro).png){style="width:400px;"}

New options for look development with Cycles and Eevee.

  

## [Grease Pencil](grease_pencil.md)

![](../../images/New_Brushes.png){style="width:400px;"}

User interface, tools, operators, modifiers, new brushes and presets and
material self overlap.

  

## [Sculpt & Retopology](sculpt.md)

New sculpt tools, poly build tool, voxel remesh and quad remesh.

## [Transform & Snapping](transform.md)

![](../../images/Snap_Perpendicular.png){style="width:200px;"}

Transform origins, new snapping, mirroring and auto merge options.

  

## [User Interface](ui.md)

![](../../images/2_81_file_browser_vertical_list.png){style="width:400px;"}

Outliner improvements, new file browser and batch rename.

  

## [Library Overrides](library_overrides.md)

A new system to replace proxies, to make local overrides to linked
characters and other data types.

## [Animation & Rigging](rigging.md)

Finer control over rotations and scale in bones, constraints and
drivers.

## [More Features](more_features.md)

Alembic, audio & video output, library management and video sequencer
editor.

## [Python API](python_api.md)

Python version upgrade, dynamic tooltip for operators, new handlers and
other API changes.

## [Add-ons](add_ons.md)

Enabled add-ons only, glTF, FBX, import images as planes, blenderkit,
rigify among others.

  
