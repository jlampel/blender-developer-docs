# Blender 2.83: Virtual Reality

Initial native virtual reality (VR) support is now available. The
feature set is focused on specific use cases for scene inspection, so it
is intentionally limited. The development team considers this to be the
**first milestone** and base for more feature-rich VR support.
(blender/blender@a22573e24,
blender/blender@3b1ef223b,
blender/blender@dc2df8307,
blender/blender@406bfd430,
blender/blender-addons@636b4ca23)

## Scene Inspection

![Blender sharing a realtime viewport with the Windows Mixed Reality Platform](../../images/VR_Windows_Mixed_Reality_Spring.png){style="width:420px;"}

Scene inspection refers to the ability to inspect the scene in VR,
without interacting with content. Essentially this means head-mounted
display (HMD) support as well as some simple settings (controllable from
outside the VR view) to control the experience.

Examples of supported use cases:

- VR as a way for directors or clients to inspect scenes and give
  feedback to artists.
- Pre-visualization for artists working on VR games.
- *Samsung GearVR*/*Google Cardboard* movie making and app development.

  

## Supported Platforms

![Blender sharing a realtime viewport with the Oculus Platform](../../images/VR_Oculus_Tram_Station.png){style="width:420px;"}

The Blender VR backend uses [OpenXR](https://www.khronos.org/openxr/), a
new standard for virtual, augmented and mixed reality. So a platform has
to support OpenXR to be usable with Blender.  
Unfortunately, SteamVR does **not** support OpenXR yet at this time, so
**HTC Vive and Valve Index are not supported**. A list of supported
platforms and a set up guide is available [in the Blender
Manual](https://docs.blender.org/manual/en/2.83/getting_started/configuration/hardware.html#head-mounted-displays-virtual-reality).

There is no OpenXR compatible platform in sight for macOS. Hence, all VR
features are disabled for macOS Blender builds.

  

## VR Scene Inspection Add-on

The user interface for the VR features is managed through an add-on,
which is disabled by default. It can be activated in the Preferences, as
any other add-on. The new options are placed in the Sidebar of 3D Views.

Main features include:

- VR Viewport  
  Support for rendering viewports to an HMD and using positional and
  rotational information from the device to move in the virtual space.
- VR Mirror  
  Each regular 3D View can act as a VR mirror, which will make it follow
  the perspective of the VR viewer. Note that usage of this feature may
  reduce the VR view performance notably.
- VR Camera Gizmo  
  Each regular 3D View has the option *Show VR Camera* as part of the VR
  settings. It will draw a camera object like shape to indicate the
  current location and rotation of the viewer in the virtual space. Note
  that usage of this feature may reduce the VR view performance notably.
- Landmarks  
  A system to store an arbitrary amount of cameras as viewer base poses.
  The location and rotation of the active landmark camera is used to
  determine the base location and rotation of the viewer. The camera
  icon in the list defines which landmark is currently used to define
  the viewer base pose of the VR session.
- *Positional Tracking* Toggle  
  Keeps the current position (calculated based on the VR eye center
  pose) when enabled while a VR session is running.
- Debugging Tools  
  New `--debug-xr` and `--debug-xr-time` command-line options.
- Python-API  
  BPY additions to manage VR settings and query session state
  information
  ([`bpy.types.XrSessionSettings`](https://docs.blender.org/api/2.83/bpy.types.XrSessionSettings.html),
  [`bpy.types.XrSessionState`](https://docs.blender.org/api/2.83/bpy.types.XrSessionState.html)).
- Helpful Error Reporting  
  In cases like when no HMD is plugged in, or no OpenXR compatible
  runtime is found, errors are reported. The error handling was
  carefully designed to be reliable and to provide useful messages to
  the user.

  
