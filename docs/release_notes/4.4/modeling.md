# Blender 4.4: Modeling & UV

* The "Select Linked Pick" operator (`L` and `Shift-L`)
  is now supported in curves edit mode (blender/blender@c7cebf5f6d3e).
* An optimization to face corner normal calculation can give up to 15% faster playback
  in files with sharp edges or custom normals (blender/blender@59f9e93413d987d7424e095).
* Normalization in the N-panel now supports locking any number of vertex groups
  (blender/blender@d6da9710d0ebb96394ccd3f02981376f420caac0).

## Select Poles

Adds a new "Select by Trait" option to select all 3-poles, 5-poles, etc.
Given the impacts of 3 & 5-poles in topology, operator default is to
select all poles which do not have 4 edges to allow easy inspection.

(blender/blender@51eb5e2803c97eafa87d835130ec5d10896f9f44).

## Improved Vertex & Edge Dissolve

Dissolving edges may dissolve additional, unselected edges to ensure the mesh remains valid.
Previously vertices connected to these unselected edges were also dissolved.

The new behavior processes only vertices that belonged to the selected, now dissolved edges.

| Input geometry | ![](images/mesh_dissolve_input.png){ width=50% } |
| -- | -- |
| Old behavior | ![](images/mesh_dissolve_old.png){ width=50% } |
| New behavior | ![](images/mesh_dissolve_new.png){ width=50% } |

(blender/blender@dc57693b0c88b93847ec206f692bf51e0173839c).


## New Topology Influence for Join Triangles

Joining triangles to quads can now prioritize quad dominant topology,
so joining triangles favors creating quads that form a "grid".

This can be controlled via a topology influence factor.

| Input geometry | ![](images/mesh_tri_join_input.png){ width=50% } |
| -- | -- |
| Default behavior | ![](images/mesh_tri_join_default.png){ width=50% } |
| With topology influence (1.0) | ![](images/mesh_tri_join_with_topology_influence.png){ width=50% } |

(blender/blender@dc57693b0c88b93847ec206f692bf51e0173839c).
