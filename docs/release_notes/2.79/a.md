# Blender 2.79a: Bug Fixes

Changes from revision
blender/blender@e8e40b171b
to
blender/blender@4c1bed0a12,
inclusive.

Total fixed bugs: 202 (117 from tracker, 85 reported/found by other
ways).

**Note:** Nearly all commits from 2.79 are listed here (over 75% of
total), only really technical/non-user affecting ones have been skipped.

## Objects / Animation / GP

### Animation

- Fix [\#52932](http://developer.blender.org/T52932): Driver with target
  of custom property from scene fails to update
  (blender/blender@ad834085b7).
- Fix [\#52835](http://developer.blender.org/T52835): When driven IK
  influence change, ik animation have 1 frame delay
  (blender/blender@89de073ea5).

<!-- -->

- Fix unreported: Fix missing ID remapping in Action editor callback
  (blender/blender@9f6d5d679e).
- Fix unreported: [\#50354](http://developer.blender.org/T50354): Action
  length calculation added unnecessary padding if some F-Curves only
  contained a single key (on the last real frame of the action)
  (blender/blender@f887b8b230).
- Fix unreported: Fix: Undo pushes were missing for Add/Remove Driver
  Variable buttons, and Remove Driver button
  (blender/blender@08e16e0bd1).

### Grease Pencil

- Fix unreported: Fix: When transforming GP strokes in "Local" mode, the
  strokes would get obscured by the transform constraint lines
  (blender/blender@99e4c819f7).

### Objects

- Fix [\#52729](http://developer.blender.org/T52729): Decimals not
  showing over 100m or 100 feet
  (blender/blender@5384b2a6e2).
- Fix [\#52140](http://developer.blender.org/T52140): Align objects
  centers using origin for text
  (blender/blender@3a0f199aa7).

### Dependency Graph

- Fix [\#52432](http://developer.blender.org/T52432): Blender crashes
  while using Ghost (new depsgraph)
  (blender/blender@2213153c27).
- Fix [\#52741](http://developer.blender.org/T52741): Follow track with
  depth object crashes Blender with new depsgraph
  (blender/blender@d5dbe0c566).
- Fix [\#53547](http://developer.blender.org/T53547): Metaballs as dupli
  objects are not updated with the new Depsgraph
  (blender/blender@8cdda3d2ad).

<!-- -->

- Fix unreported: Transform: Enable recursion dependency check for new
  depsgraph
  (blender/blender@ef04aa4a43).
- Fix unreported: Depsgraph: Fix relations for metaballs
  (blender/blender@7103c6ef3b).

## Data / Geometry

### Armatures

- Fix [\#53300](http://developer.blender.org/T53300): Bone Extrude via
  Ctrl + Click is not done from active bone tail
  (blender/blender@969196069a).
- Fix [\#53054](http://developer.blender.org/T53054): Parentless bone +
  IK crashes
  (blender/blender@c63e08863d).

### Curve/Text Editing

- Fix [\#52860](http://developer.blender.org/T52860): 3D Text crashes w/
  Ctrl Backspace
  (blender/blender@b969f7eb55).
- Fix [\#53410](http://developer.blender.org/T53410): 3D Text always
  recalculated
  (blender/blender@480b0b5dfc).
- Fix [\#53586](http://developer.blender.org/T53586): Surfaces collapse
  when joined
  (blender/blender@1611177ac9).

<!-- -->

- Fix unreported: Curves: Fix wrong bitset being checked against CYCLIC
  bit flag
  (blender/blender@2edc2b4912).

### Mesh Editing

- Fix [\#53145](http://developer.blender.org/T53145): bevel tool fails
  when used a second time
  (blender/blender@36f324fc55).
- Fix [\#53145](http://developer.blender.org/T53145): bevel tool does
  not start with amount at zero
  (blender/blender@f1a4e130b3).
- Fix [\#52723](http://developer.blender.org/T52723): Reset UV layers
  failed
  (blender/blender@0641069778).
- Fix [\#52748](http://developer.blender.org/T52748): Select shortest
  face path fails
  (blender/blender@60bfa969e2).
- Fix [\#52384](http://developer.blender.org/T52384): Bridge pair result
  depends on other loops
  (blender/blender@495aa77b53).
- Fix [\#53131](http://developer.blender.org/T53131): Incorrect
  vert-edge angle calculation
  (blender/blender@60e8d86fb1).
- Fix [\#53441](http://developer.blender.org/T53441): Inset doesn't
  start at zero
  (blender/blender@19b27d84ab).
- Fix [\#53529](http://developer.blender.org/T53529): Rip crashes w/
  wire edge
  (blender/blender@52a5daa404).
- Fix [\#53343](http://developer.blender.org/T53343): Custom Normal Data
  Transfer Crashes when some vertexes have no faces
  (blender/blender@ff8c9c5931).
- Fix [\#53420](http://developer.blender.org/T53420): Vertex Groups: The
  "-" button gets a hidden function
  (blender/blender@50ca70b275).
- Fix [\#52733](http://developer.blender.org/T52733): Percent mode for
  Bevel sometimes had nans
  (blender/blender@48079e1f11).
- Fix [\#52871](http://developer.blender.org/T52871):
  `BLI_polyfill_beautify_quad_rotate_calc_ex` was mistakenly
  considering the state as degenerated
  (blender/blender@9298d99b77).
- Fix [\#52871](http://developer.blender.org/T52871): beauty fill error
  (blender/blender@9d501d23cb).
- Fix [\#53143](http://developer.blender.org/T53143): Knife Crash after
  Grid Fill
  (blender/blender@4f247ed07a).
- Fix [\#53713](http://developer.blender.org/T53713): User remap failed
  w/ texface images
  (blender/blender@26ffade5c1).
- Fix [\#52818](http://developer.blender.org/T52818): Tangent space
  calculation is really slow for high-density mesh with degenerated
  topology
  (blender/blender@9a5320aea3).
- Fix [\#53678](http://developer.blender.org/T53678): Smart Project UV
  margin ignores units
  (blender/blender@42e207b599).

<!-- -->

- Fix unreported: Fix for inset when accessed from spacebar search
  (blender/blender@db3e3f9c24).
- Fix unreported: Edit Mesh: click extrude, ensure inverse matrix
  (blender/blender@3e3a27d089).
- Fix unreported: Polyfill Beautify: option to rotate out of degenerate
  state
  (blender/blender@0834eefae0).
- Fix unreported: BMesh: use less involved check for edge rotation
  (blender/blender@58ab62ed6d).
- Fix unreported: Avoid bias when calculating quad split direction
  (blender/blender@569d2df634).
- Fix unreported: Beauty fill was skipping small faces
  (blender/blender@1b6130533f).
- Fix unreported: Polyfill Beautify: half-edge optimization
  (blender/blender@4a457d4f1e).
- Fix unreported: Use BLI_heap_reinsert for decimate and beautify
  (blender/blender@eaeb0a002e).
- Fix unreported: BMesh: move bridge tools stepping logic into macro
  (blender/blender@02b206780e).
- Fix unreported: Subsurf: Avoid global lock for loops and orig index
  layers
  (blender/blender@83b0603061).
- Fix unreported: Subsurf: Avoid possible use of partially initialized
  edge hash
  (blender/blender@72151f3e36).
- Fix unreported: Fix scalability issue in threaded code of Mesh normals
  computation
  (blender/blender@71e0894e0d).

### Modifiers

- Fix [\#51074](http://developer.blender.org/T51074): Boolean modifier
  inverts operation
  (blender/blender@d07011da71).
- Fix [\#52291](http://developer.blender.org/T52291): Boolean fails w/
  co-linear edged ngons
  (blender/blender@78cc3c828f).
- Fix [\#52763](http://developer.blender.org/T52763): Boolean problem
  with vertex group
  (blender/blender@00d8097510).
- Fix [\#52823](http://developer.blender.org/T52823): New Depsgraph -
  Shrinkwrap crashes blender
  (blender/blender@754630cee4).
- Fix [\#53007](http://developer.blender.org/T53007): OpenSubdiv +
  transparency = artefact/crashes
  (blender/blender@ba40d8f331).
- Fix [\#53398](http://developer.blender.org/T53398): Surface deform
  modifier says that convex polygons are concave for big faces
  (blender/blender@4c46f69376).

<!-- -->

- Fix unreported: Fix (irc-reported by @sergey) invalid precision value
  in a float RNA property
  (blender/blender@8e43a9e9cc).

### Material / Texture

- Fix [\#53116](http://developer.blender.org/T53116): default texture
  coordinates for volume materials are blank
  (blender/blender@576899b90a).
- Fix [\#52953](http://developer.blender.org/T52953): Crash removing
  material
  (blender/blender@243b961c29).
- Fix [\#53509](http://developer.blender.org/T53509): Datablock ID
  Properties attached to bpy.types.Material are not loaded
  (blender/blender@0c365472b6).

<!-- -->

- Fix unreported: Fix logic for pinning textures users from context
  (blender/blender@8f9d9ba14e).

## Physics / Simulations / Sculpt / Paint

### Particles

- Fix [\#53552](http://developer.blender.org/T53552): Unneeded particle
  cache reset on frame change
  (blender/blender@6f19787e52).
- Fix [\#52732](http://developer.blender.org/T52732): Particle system
  volume grid particles out of volume
  (blender/blender@17c00d222f).
- Fix [\#53513](http://developer.blender.org/T53513): Particle size
  showing in multiple places
  (blender/blender@c70a45027d).
- Fix [\#53832](http://developer.blender.org/T53832): Particle weight
  paint crash
  (blender/blender@5b3538e02a).
- Fix [\#53823](http://developer.blender.org/T53823): Particle weight
  brush crash
  (blender/blender@b6481cbbe5).

<!-- -->

- Fix unreported: Fix missing update for particles w/ fluids
  (blender/blender@84361a0709).

### Physics / Hair / Simulations

- Fix [\#53650](http://developer.blender.org/T53650): remove hard limits
  on force field size and max distance
  (blender/blender@2a9abc0f5e).

<!-- -->

- Fix unreported: Fix error copying smoke modifier uv layer
  (blender/blender@9f032c3867).
- Fix unreported: Depsgraph: Don't make non-dynamic hair dependent on
  time
  (blender/blender@76032b133c).

### Sculpting / Painting

- Fix [\#53577](http://developer.blender.org/T53577): Rake sculpt/paint
  wrong on first step
  (blender/blender@413817a3d2).
- Fix [\#52537](http://developer.blender.org/T52537): Dyntopo "detail
  flood fill" doesn't work in some cases
  (blender/blender@1b8e8326b4).
- Fix [\#53593](http://developer.blender.org/T53593): sculpt brush rake
  spacing bug after recent bugfix
  (blender/blender@4c1bed0a12).

<!-- -->

- Fix unreported: Fix brush reset (missing notifier)
  (blender/blender@98dc9072e5).
- Fix unreported: Fix sculpt secondary color missing some brushes
  (blender/blender@6058b651c4).

## Image / Video / Render

### Image / UV Editing

- Fix [\#53092](http://developer.blender.org/T53092): errors reading EXR
  files with different data/display window
  (blender/blender@f901bf6f47).
- Fix [\#52739](http://developer.blender.org/T52739): Crash loading
  corrupted video files
  (blender/blender@9c39f021ad).
- Fix [\#52811](http://developer.blender.org/T52811): At any framerate
  selected, video exported with 1000fps
  (blender/blender@3163d0e205).
- Fix [\#52920](http://developer.blender.org/T52920): Saving Tiff Files
  type Blender crashes
  (blender/blender@d305c10104).
- Fix [\#53499](http://developer.blender.org/T53499): Cannot load DPX
  files
  (blender/blender@b8bdca8c0a).

<!-- -->

- Fix unreported: Fix writing Iris images w/ invalid header
  (blender/blender@acae901a10).

### Masking

- Fix [\#52840](http://developer.blender.org/T52840): New Depsgraph -
  Mask editor not working correctly
  (blender/blender@69062cdd8d).
- Fix [\#52749](http://developer.blender.org/T52749): New Depsgraph -
  Render View Mask is not initialized correctly
  (blender/blender@538182511a).
- Fix [\#53419](http://developer.blender.org/T53419): Masking "Add" menu
  is not present in Image editor, but shortcut is
  (blender/blender@c8f95c7829).

### Motion Tracking

- Fix [\#52851](http://developer.blender.org/T52851): Per-frame traking
  is broken when sequence doesn't start at frame 1
  (blender/blender@f0743b56ec).
- Fix [\#53612](http://developer.blender.org/T53612): Blender crashes on
  CleanTracks with 'DELETE_SEGMENTS' and a disabled marker
  (blender/blender@a1d05ac2a1).

<!-- -->

- Fix unreported: Tracking: Fix crash when tracking failed
  (blender/blender@350f5c6894).
- Fix unreported: Tracking: Create mesh from selected tracks only
  (blender/blender@333ef6e6f7).

### Nodes / Compositor

- Fix [\#53360](http://developer.blender.org/T53360): crash with GLSL
  bump mapping and missing group output node
  (blender/blender@bb89759624).
- Fix [\#52299](http://developer.blender.org/T52299): X resolution of 4
  causes nodes to collapse
  (blender/blender@cdc35e63bd).
- Fix [\#53371](http://developer.blender.org/T53371): Keying Node fails
  with values above 1
  (blender/blender@1c7657befb).
- Fix [\#52113](http://developer.blender.org/T52113): Compositor doesnt
  mix unrendered render layers well
  (blender/blender@3182336666).
- Fix [\#53263](http://developer.blender.org/T53263): Proposed Blender
  crashes when rendering with Stabilizer 2D node without movie selected
  (blender/blender@ce35151f38).
- Fix [\#53696](http://developer.blender.org/T53696): Compositor HSV
  limits changed
  (blender/blender@528e00dae0).
- Fix [\#52927](http://developer.blender.org/T52927): Compositor wrong
  scale when scale size input is connected to complex node
  (blender/blender@259e9ad00d).
- Fix [\#47212](http://developer.blender.org/T47212): Luminance Key not
  working with HDR and out-of-gamut ranges
  (blender/blender@13973a5bbf).

<!-- -->

- Fix unreported: Fix compositor node links getting lost on file load
  for custom render passes
  (blender/blender@6bd1189e5c).
- Fix unreported: Node selection: Stop operator when mouse selection
  selected a node
  (blender/blender@bf58ec9265).
- Fix unreported: Compositor: Ensured 16 byte alignment for variables
  accessed by SSE instructions
  (blender/blender@02bac54fa9).
- Fix unreported: (Nodes) Display image name if any in the Image and
  Texture Image node title
  (blender/blender@010cf35e7e).
- Fix unreported: Fix old files with changed node socket type not
  loading correctly
  (blender/blender@79563d2a33).

### Render

- Fix [\#52679](http://developer.blender.org/T52679): Hole in bake
  normal
  (blender/blender@759af7f1ee).
- Fix [\#53185](http://developer.blender.org/T53185): After rendering an
  animation (Ctrl-F12), pressing F12 no longer renders single frames
  only
  (blender/blender@6cfc5edb86).
- Fix [\#53810](http://developer.blender.org/T53810): Crash removing a
  scene used in render
  (blender/blender@5ddcad4377).

<!-- -->

- Fix unreported: Fix incorrect color management when saving JPG
  previews for EXR
  (blender/blender@1dbdcfe9a9).
- Fix unreported: Fix leak when rendering OpenGL animations
  (blender/blender@e4dce3b3d7).

### Render: Cycles

- Fix [\#53129](http://developer.blender.org/T53129): Cycles missing
  update when changing image auto refresh
  (blender/blender@a6e5558194).
- Fix [\#53217](http://developer.blender.org/T53217): GLSL principled
  BSDF black with zero clearcoat roughness
  (blender/blender@d564beda44).
- Fix [\#52368](http://developer.blender.org/T52368): Cycles OSL trace()
  failing on Windows 32 bit
  (blender/blender@2bc667ec79).
- Fix [\#53348](http://developer.blender.org/T53348): Cycles difference
  between gradient texture on CPU and GPU
  (blender/blender@198fd0be43).
- Fix [\#53171](http://developer.blender.org/T53171): lamp specials
  strength tweak fails with renamed emission nodes
  (blender/blender@5b2d5f9077).
- Fix [\#52573](http://developer.blender.org/T52573): Cycles baking
  artifacts
  (blender/blender@b0c55d5c94).
- Fix [\#51416](http://developer.blender.org/T51416): Blender Crashes
  while moving Sliders
  (blender/blender@aafe528c7d).
- Fix [\#53048](http://developer.blender.org/T53048): OSL Volume is
  broken in Blender 2.79
  (blender/blender@ad7385422e).
- Fix [\#53559](http://developer.blender.org/T53559): Auto texture space
  for text and font is wrong in Cycles
  (blender/blender@e7aad8fd0e).
- Fix [\#53600](http://developer.blender.org/T53600): Cycles shader
  mixing issue with principled BSDF and zero weights
  (blender/blender@836a1ccf72).
- Fix [\#52801](http://developer.blender.org/T52801): reload scripts
  causes Cycles viewport render crash
  (blender/blender@f1ee24a284).
- Fix [\#53017](http://developer.blender.org/T53017): Cycles not
  detecting AMD GPU when there is an NVidia GPU too
  (blender/blender@1f50f0676a).
- Fix [\#53755](http://developer.blender.org/T53755): Cycles OpenCL lamp
  shaders have incorrect normal
  (blender/blender@2ca933f457).
- Fix [\#53692](http://developer.blender.org/T53692): OpenCL multi GPU
  rendering not using all GPUs
  (blender/blender@30a0459f2c).
- Fix [\#53567](http://developer.blender.org/T53567): Negative pixel
  values causing artifacts with denoising
  (blender/blender@824c039230).
- Fix [\#53012](http://developer.blender.org/T53012): Shadow catcher
  creates artifacts on contact area
  (blender/blender@c3237cdc13).

<!-- -->

- Fix unreported: OpenVDB: Fix compilation error against OpenVDB 4
  (blender/blender@0cdfe887b4).
- Fix unreported: Cycles: Fix possible race condition when generating
  Beckmann table
  (blender/blender@b5c629e604).
- Fix unreported: Fix Cycles bug in RR termination, probability should
  never be \> 1.0
  (blender/blender@df1af9b349).
- Fix unreported: CMake: support CUDA 9 toolkit, and automatically
  disable sm_2x binaries
  (blender/blender@cd9c68f0c5).
- Fix unreported: Fix part of
  [\#53038](http://developer.blender.org/T53038): principled BSDF
  clearcoat weight has no effect with 0 roughness
  (blender/blender@440d647cc1).
- Fix unreported: Fix incorrect MIS with principled BSDF and specular
  roughness 0
  (blender/blender@866be3423c).
- Fix unreported: Cycles: Fix possible race condition when initializing
  devices list
  (blender/blender@93d711ce55).
- Fix unreported: Cycles: Fix wrong shading when some mesh triangle has
  non-finite coordinate
  (blender/blender@075950ad66).
- Fix unreported: Cycles: Fix compilation error with latest OIIO
  (blender/blender@49f57e5346).
- Fix unreported: Cycles: Fix compilation error with OIIO compiled
  against system PugiXML
  (blender/blender@b6f3fec259).
- Fix unreported: Cycles: Fix compilation error of standalone
  application
  (blender/blender@391f7cc406).
- Fix unreported: Cycles: Workaround for performance loss with the CUDA
  9.0 SDK
  (blender/blender@b3adce7766).
- Fix unreported: Cycles: Fix difference in image Clip extension method
  between CPU and GPU
  (blender/blender@a50c381fac).
- Fix unreported: Cycles: Fix crash opening user preferences after
  adding extra GPU
  (blender/blender@a3616980c6).
- Fix unreported: Cycles: Fix bug in user preferences with factory
  startup
  (blender/blender@2f5a027b94).

### Sequencer

- Fix [\#52890](http://developer.blender.org/T52890): Crash unlinking
  sequencer sound
  (blender/blender@eaba111bd5).
- Fix [\#53430](http://developer.blender.org/T53430): Cut at the strip
  end fails w/ endstill
  (blender/blender@5ab1897de7).
- Fix [\#52940](http://developer.blender.org/T52940): VSE Glow Effect
  Strip on transparent images has no blur
  (blender/blender@3ad84309df).
- Fix [\#53639](http://developer.blender.org/T53639): text sequence
  strips no stamped into render
  (blender/blender@8f7030e5f3).

## UI / Spaces / Transform

### 3D View

- Fix [\#52959](http://developer.blender.org/T52959): Local view looses
  clip range on exit
  (blender/blender@7f6e9c595b).
- Fix [\#53850](http://developer.blender.org/T53850): Lock to Cursor
  breaks 3D manipulators
  (blender/blender@b90f3928a3).

<!-- -->

- Fix unreported: Fix ruler access from search pop-up
  (blender/blender@b0fbe95a1b).
- Fix unreported: 3D View: use shortest angle between quaternions
  (blender/blender@2895cb22a7).

### Input (NDOF / 3D Mouse)

- Fix [\#52998](http://developer.blender.org/T52998): disabled menu
  entries responding to key shortcuts
  (blender/blender@20c96cce86).
- Fix [\#52861](http://developer.blender.org/T52861): Keymap editor
  filter doesn't show shortcuts using "+"
  (blender/blender@ca236408f3).

### Outliner

- Fix [\#53342](http://developer.blender.org/T53342): Outliner 'select
  hierarchy' broken
  (blender/blender@7153cee305).

### Transform

- Fix [\#53463](http://developer.blender.org/T53463): Rotation numerical
  input shows unstable behavior
  (blender/blender@8a9d64b578).
- Fix [\#53309](http://developer.blender.org/T53309): Remove default
  'Clear loc/rot/scale delta transform' shortcuts
  (blender/blender@c4f8d924e1).
- Fix [\#52086](http://developer.blender.org/T52086): Graph editor
  "normalize" drag errors for integers
  (blender/blender@9f916baa70).
- Fix [\#53311](http://developer.blender.org/T53311): transform
  edge/normal orientation
  (blender/blender@57b11d8b4e).

### User Interface

- Fix [\#52800](http://developer.blender.org/T52800): fix UI flickering
  with Mesa on Linux
  (blender/blender@c85a305c09).
- Fix [\#53630](http://developer.blender.org/T53630): Effect strips not
  disFix [\#52977](http://developer.blender.org/T52977): playing Input
  data. Parent bone name disappeared in the UI in pose mode
  (blender/blender@4bc89d815c).

<!-- -->

- Fix unreported: Fix failure in our UI code that could allow search
  button without search callbacks, leading to crash
  (blender/blender@e8e40b171b).
- Fix unreported: Add some security checks against future bad float
  UIprecision values
  (blender/blender@7e4be7d348).
- Fix unreported: UI: avoid int cast before clamping number input
  (blender/blender@a437b9bbbe).
- Fix unreported: UI: fullstop at end of tooltips
  (blender/blender@4c3e2518c8).
- Fix unreported: Fix incorrect allocation size
  (blender/blender@6b2d1f63db).

## Game Engine

- Fix unreported: Fix BGE sound actuator property access
  (blender/blender@d2d207773e).

## System / Misc

### Collada

- Fix [\#53322](http://developer.blender.org/T53322): Collada export
  crash w/ shape keys
  (blender/blender@869e4a3420).
- Fix [\#53230](http://developer.blender.org/T53230): avoid Nullpointer
  problems in Collada Exporter
  (blender/blender@eaf14b1ebf).
- Fix [\#52831](http://developer.blender.org/T52831): removed
  enforcement of matrix decomposition when animations are exported
  (blender/blender@a123dafdc0).

### File I/O

- Fix [\#52816](http://developer.blender.org/T52816): regression can't
  open file in 2.79 (crash)
  (blender/blender@a4116673c7).
- Fix [\#53002](http://developer.blender.org/T53002): Batch-Generate
  Previews generate empty or none image for large objects
  (blender/blender@47a388b2a9).
- Fix [\#52514](http://developer.blender.org/T52514): don't clear
  filename when dropping directory path in file browser
  (blender/blender@625b2f5dab).
- Fix [\#53250](http://developer.blender.org/T53250): Crash when
  linking/appending a scene to a blend when another linked scene in this
  blend is currently open/active
  (blender/blender@06df30a415).
- Fix [\#53572](http://developer.blender.org/T53572): Alembic imports UV
  maps incorrectly
  (blender/blender@0f841e24b0).

<!-- -->

- Fix unreported: Alembic import: fixed mesh corruption when changing
  topology
  (blender/blender@bae796f0d5).

### Other

- Fix [\#53274](http://developer.blender.org/T53274): Saving template
  prefs overwrites default prefs
  (blender/blender@38fdfe757d).
- Fix [\#53637](http://developer.blender.org/T53637): Keymap from
  app-template ignored
  (blender/blender@8c484d0bda).

<!-- -->

- Fix unreported: Fix potential string buffer overruns
  (blender/blender@30f53d56b6).
- Fix unreported: Fix build with OSL 1.9.x, automatically aligns to 16
  bytes now
  (blender/blender@f968268c1e).
- Fix unreported: WM: minor correction to user-pref writing
  (blender/blender@75aec5eeaa).
- Fix unreported: WM: don't load preferences on 'File -\> New'
  (blender/blender@571e801b27).
- Fix unreported: WM: load UI for new file, even when pref disabled
  (blender/blender@9f0ebb0941).
- Fix unreported: Fix MSVSC2017 error
  (blender/blender@405874bd79).
- Fix unreported: Fix SGI foramt reader CVE-2017-2901
  (blender/blender@7b8b621c1d).
- Fix unreported: Memory: add MEM_malloc_arrayN() function to protect
  against overflow
  (blender/blender@a972729895).
- Fix unreported: Fix buffer overflows in TIFF, PNG, IRIS, DPX, HDR and
  AVI loading
  (blender/blender@16718fe4ea).
- Fix unreported: Fix buffer overflow vulernability in thumbnail file
  reading
  (blender/blender@04c5131281).
- Fix unreported: Fix buffer overflow vulnerabilities in mesh code
  (blender/blender@9287434fa1).
- Fix unreported: Fix buffer overflow vulnerability in curve, font,
  particles code
  (blender/blender@8dbd5ea4c8).
- Fix unreported: Fix manual lookups (data is now lowercase)
  (blender/blender@cae8c68ca6).

### Python

- Fix [\#53273](http://developer.blender.org/T53273): render bake
  settings properties not showing correct Python path
  (blender/blender@825aecaee3).
- Fix [\#52442](http://developer.blender.org/T52442):
  bl_app_templates_system not working
  (blender/blender@d89353159f).
- Fix [\#53294](http://developer.blender.org/T53294): bpy.ops.image.open
  crash
  (blender/blender@8ea3f6438d).
- Fix [\#53191](http://developer.blender.org/T53191): Python API
  Reference link wrong in splash screen
  (blender/blender@5f2307d1a7).
- Fix [\#52982](http://developer.blender.org/T52982): Join operator with
  context override crashes Blender 2.79
  (blender/blender@09c387269a).
- Fix [\#53772](http://developer.blender.org/T53772): Presets don't
  support colons
  (blender/blender@91ce295796).

<!-- -->

- Fix unreported: Fix bpy.utils.resource_path('SYSTEM') output
  (blender/blender@21ecdacdf1).
- Fix unreported: Fix setting the operator name in Py operator API
  (blender/blender@8c9bebfe28).
- Fix unreported: Docs: add note for bmesh face_split_edgenet
  (blender/blender@a253b1b7bc).
- Fix unreported: Fix edge-split bmesh operator giving empty result
  (blender/blender@ce6e30b3dc).
- Fix unreported: Fix BMesh PyAPI internal flag clearing logic
  (blender/blender@6639350f92).
- Fix unreported: Docs: clarify return value for BVH API
  (blender/blender@0c456eb90a).
- Fix unreported: bl_app_override: support empty UI layout items
  (blender/blender@6d640504e8).
- Fix unreported: Fix bad 'poll' prop callback API doc
  (blender/blender@3193045c59).
- Fix unreported: Fix background_job template
  (blender/blender@38357cd004).
- Fix unreported: Fix bmesh.utils.face_join arg parsing
  (blender/blender@20cccb1561).

### System

- Fix [\#53004](http://developer.blender.org/T53004): XWayland ignores
  cursor-warp calls
  (blender/blender@1ab828d4bb).
- Fix [\#53068](http://developer.blender.org/T53068): AMD Threadripper
  not working well with Blender
  (blender/blender@dfed7c48ac).

<!-- -->

- Fix unreported: GPU: Fix memory corruption in GPU_debug on GTX1080
  (blender/blender@3a1ade22e5).
- Fix unreported: Task scheduler: Start with suspended pool to avoid
  threading overhead on push
  (blender/blender@8553449cf2).

<hr/>
