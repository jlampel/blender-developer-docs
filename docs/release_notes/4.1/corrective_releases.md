# Blender 4.1: Corrective Releases

## Blender 4.1.1

Released on April 16th 2024, Blender 4.1.1 features 58 bug fixes:

- Active curve vertex/spline not visualizing properly
  blender/blender#119907
- Anim properties are not highlighted in tree view
  blender/blender#120278
- Anim: fix animation paths when renaming bone collections
  blender/blender@e2d2281b1ff4e04932cdc20404f6c19640e60ccc
- Anim/Drivers on Bone Collections created in 4.0 break in 4.1
  blender/blender#120447
- Area lamp artifacts in Cycles with light trees
  blender/blender#120119
- Attach to root panel if new parent is null
  blender/blender#120367
- Baking simulation only bakes a single frame
  blender/blender@158b2fc28b97b571221004d5a8b35236f7352499
- Cage face normals overlay always draws original normals
  blender/blender#120154
- Crash reading large jpeg
  blender/blender#120113
- Crash rendering in sculpt mode with multires active
  blender/blender#119969
- Crash rendering when using OptiX and Grease Pencil on recent drivers
  blender/blender#120007
- Crash selecting in mask editor
  blender/blender#119936
- Crash when baking during animation
  blender/blender#119958
- Crash with single point NURBS curve
  blender/blender#117709
- Custom group node remains undefined after registering node type
  blender/blender#120084
- Cycles NEE not excluding self intersection
  blender/blender#119813
- Cycles render issue with light tree and light linking
  blender/blender#119692
- Cycles: Properly default to Metal-RT off unless GPU is a M3 or newer
  blender/blender#120126 blender/blender#120006
- Duplicate auto smooth modifier added by versioning in one case
  blender/blender#120030
- Enabling "Distribute memory between devices" for Cycles results in error
  blender/blender#119959
- Escape property name when keying
  blender/blender@9b750f42b839ee4109eaa8013b1c25675f619594
- File Output node always has inputs of type Color
  blender/blender#120199
- File Output node has wrong BW output
  blender/blender#120175
- File rename fails on Mac with certain filesystems
  blender/blender#119966
- Fix buffer overflow from passing undersized buffers to BLI_path_abs
  blender/blender@50aa9357efb23f29f14886f35257b7d06e58895e
- Fix buffer overflow with BLI_str_format_uint64_grouped
  blender/blender@776327832d30438b5c4a84492bfb34bf99af0d04
- Fix error copying a grease pencil strokes fill opacity
  blender/blender@a3817e9f88e7196e78cbee51f05917cd1c73bbf5
- Fix sizeof(sizeof(...)) passed to BLI_path_slash_ensure
  blender/blender@21031af49c4470d78ca30c987d0454904dc5d9ab
- Flip node produces artifacts blender/blender#119973
- Geometry Nodes: Avoid repeated node tools lookup when empty
  blender/blender#120494
- glTF Exporter: Crash exporting instanced collection when lamp option is enabled
  blender/blender-addons@e08c38b02564f07eb9f0f01258b513eb0e511e05
- Invert Visible doesn't invert Face Sets properly
  blender/blender#120144
- Keying Set export fails in python
  blender/blender#120470
- Many Outliner operators crash running without a region
  blender/blender#112618
- Mask property keyframes missing when reload the file
  blender/blender#119925
- Memory leak in volume grid handling
  blender/blender#120402
- NLA stack decomposition doesn't work with bones
  blender/blender#119946
- Object transform ignored for shrinkwrap and flat shading
  blender/blender#119992
- Over-allocation in multires sculpt undo nodes
  blender/blender#120187
- Performance regression when adding output attribute
  blender/blender#119938
- Persistent state not set when disabling add-ons
  blender/blender#119664
- Remove cyclic dependency of pbvh type when building bmesh pbvh
  blender/blender#120400
- Remove from Vertex Group operator broken
  blender/blender#120309
- Remove links to and from unsupported socket types
  blender/blender#119554
- Sculpt paint crash after converting active color attribute
  blender/blender@2ece208f2e8d3fd2e688b4b17d5b5b284ad6bf6a
- Sculpting is slow with modifier with driver
  blender/blender#120200
- Set Object runtime matrices to identity when loading 4.2 files
  blender/blender@d66b42e382ecb1436c3ae2c8a33724cb802a0ff8
- Texture paint sampling broken with modifiers
  blender/blender#117338
- UI List search broken when class names >32 chars
  blender/blender#114667
- Undoing a rename while in edit mode crashes Blender
  blender/blender#120058
- Unhandled empty Optional in 'bounds_min_max()'
  blender/blender@f15314464c7556070dd84e51e9c36a03e6146bb6
- Unkeyable custom properties receive keyframes
  blender/blender#119909
- USD animated primvar import
  blender/blender@ad193c613547bc6b3551884dda4a82e245a51466
- USD Cache file operators now recognize USD as well as Alembic
  blender/blender#99114
- UV Sync-select selects faces instead of just edges
  blender/blender#117320
- Video output artifacts due to threaded YUV conversion
  blender/blender#120077
- VSE crash rendering a scene strip with missing Editing data
  blender/blender#120417
- Wrong node selection after duplicating node
  blender/blender#120087
