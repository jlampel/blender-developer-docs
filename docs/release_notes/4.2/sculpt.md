# Blender 4.2 LTS: Sculpt, Paint, Texture

## Sculpting

### New Tools

<figure>
  <img src="../images/sculpt_tools.png" alt="Sculpt Tools">
  <figcaption>Trim, Mask, Hide, and Face Set sculpt tools in Blender 4.2 LTS</figcaption>
</figure>

All Trim, Face Set, Mask, and Hide tools now have Line and Polyline
counterparts, allowing for a quick way to create polygonal shapes.

- **Polyline Mask** -
  [Manual](https://docs.blender.org/manual/en/4.2/sculpt_paint/sculpting/tools/mask_tools.html#polyline-mask)
  (blender/blender@025df21a0f7e21e164fe82ab4e22159836ea6675)
- **Polyline Hide** -
  [Manual](https://docs.blender.org/manual/en/4.2/sculpt_paint/sculpting/tools/hide_tools.html#polyline-hide)
  (blender/blender@55fc1066acf69dd04f7f7b1c3af4c3f360769523)
- **Polyline Face Set** -
  [Manual](https://docs.blender.org/manual/en/4.2/sculpt_paint/sculpting/tools/face_set_tools.html#polyline-face-set)
  (blender/blender@025df21a0f7e21e164fe82ab4e22159836ea6675)
- **Polyline Trim** -
  [Manual](https://docs.blender.org/manual/en/4.2/sculpt_paint/sculpting/tools/trim_tools.html#polyline-trim)
  (blender/blender@025df21a0f7e21e164fe82ab4e22159836ea6675)
  <figure>
    <video src="../videos/sculpt_polyline_hide.mp4"
          title="Demo of Polyline Hide tool" width="350" controls=""></video>
    <figcaption>Polyline Hide</figcaption>
  </figure>


- **Line Hide** -
  [Manual](https://docs.blender.org/manual/en/4.2/sculpt_paint/sculpting/tools/hide_tools.html#line-hide)
  (blender/blender@6e997cc75723b0dfc4d25d8246f373d46a662905)
- **Line Face Set** -
  [Manual](https://docs.blender.org/manual/en/4.2/sculpt_paint/sculpting/tools/face_set_tools.html#line-face-set)
  (blender/blender@7726e7f563190620034f43a19aa01b9907a26530)
- **Line Trim** -
  [Manual](https://docs.blender.org/manual/en/4.2/sculpt_paint/sculpting/tools/trim_tools.html#line-trim)
  (blender/blender@d4a61647bf571cb90090e1320f4aa1156482836f)


- **Lasso Hide** -
  [Manual](https://docs.blender.org/manual/en/4.2/sculpt_paint/sculpting/tools/hide_tools.html#lasso-hide)
  (blender/blender@68afd225018e59d63431e02def51575bfc76f5cb)


- **Grow & Shrink Visibility**
  (blender/blender@1ab3fffc1d5fa81d95f4d05a46d2f7ccf8fb1f93)

<figure>
<video src="../videos/sculpt_grow_shrink_visibility.mp4"
       title="Demo of Grow/Shrink Visibility operator" width="350" controls=""></video>
<figcaption>Grow/Shrink Visibility</figcaption>
</figure>

### New Options

- Trim tools now can use either **Fast** or **Exact** solvers.
  ([documentation](https://docs.blender.org/manual/en/4.2/sculpt_paint/sculpting/tools/trim_tools.html#tool-settings))
  (blender/blender@881178895b8528fd37ef41cd7a4bc481f7b7d311)

## Other

- Sculpt and Weight Paint modes now use the global, customizable rotation
  increment for corresponding Line tools when snapping is enabled.
  For example Line Hide in Sculpt and Gradient in Weight Paint.
  (blender/blender@1cf0d7ca6a858b5c7a0b03a224d9e826ffa38c47)
