# Blender 4.2 LTS: EEVEE

The EEVEE render engine was rewritten to allow deeper changes, removing long standing limitations and facilitating future evolution.

## Migrating from Older Versions
Most scenes should be automatically adjusted to keep the same look. However, there are some cases that need manual adjustments.
See **[migration process](eevee_migration.md)** for more details.

## Global Illumination
EEVEE now uses screen space ray tracing for every BSDF. There is no longer any limitation to the number of BSDFs.

Materials using the `Blended` render method are now removed from reflections instead of being projected onto the background. [See the manual](https://docs.blender.org/manual/en/4.2/render/eevee/render_settings/raytracing.html#fast-gi-approximation).

## Lights
There is no longer a limit to the number of lights in a scene. However, only 4096 lights can be visible at the same time.

- Lights are now visible through refractive surfaces. A new transmission influence factor has been added and set to 0 for older files.
- Lights now support ray visibility options.
- Glossy lighting no longer leaks at the back of objects.

## Shadows
Shadows are now rendered using Virtual Shadow Maps. This greatly increases the maximum resolution, reduces biases and simplifies the setup.

- Light visibility is now computed using Shadow Map Ray Tracing, providing plausible soft shadows without needing shadow jittering.
- Shadow Map biases have been removed and are now computed automatically. A workaround for the shadow terminator issue is planned for future releases.
- Contact shadows have been removed since Virtual Shadow Maps are precise enough in most cases.
- Shadow clip start has been removed and replaced by an automatic value.

## Shading
### Shading Modes
`Material > Blend Mode` was replaced by `Material > Render Method`. `Blended` corresponds to the former `Alpha Blend`.
`Material > Shadow Mode` was replaced by `Object > Visibility > Ray Visibility > Shadow` at the object level.

A `Backface Culling` option for shadow and a `Transparent Shadows` option were added to reduce the performance impact of rendering Virtual Shadow Maps.

Shadows and surfaces with the `Dithered` render method behave the same as the former `Alpha Hashed` method. To reproduce the same behavior as the former `Alpha Clip` option, materials need be modified by adding a `Math` node with mode `Greater Than`.

`Blended` materials now have correct rendering ordering of their opaque pixels.

The `Screen-Space Refraction` option was renamed to `Raytraced Transmission` and affects all transmissive BSDF nodes (Translucent BSDF, Subsurface Scattering, Refraction BSDF, ...).

### Displacement
Displacement is now supported with the exception of the `Displacement Only` mode which falls back to `Displacement And Bump`. [See the manual](https://docs.blender.org/manual/en/4.2/render/materials/components/displacement.html#displacement-and-bump)

### Thickness
A new Thickness output has been introduced. This allows better modeling of Refraction, Subsurface Scattering and Translucency. Some materials might need adjustment to keep the same appearance. This replaces the former `Refraction Depth` option. (blender/blender!120384)

### Subsurface Scattering
The new Subsurface Scattering implementation supports any number of BSSRDF nodes with arbitrary radii.
Subsurface Scattering no longer leaks between objects and has no energy loss.

Subsurface translucency is always computed and the associated option has been removed.

## Light Probes
The render panel options have been split between the light probe data panel and the world data panel.

### Volume Light Probes
The new volume light probes baking converges faster with higher resolutions.

Baking is now done at the object level, allowing volume light probes to be edited after baking and linked from other files. Optionally they can bake sky visibility for dynamic sky lighting.

There is a new rejection algorithm and flood fill to reduce shadow leaking.

Volume light probes now affect volume shading. However object volumes and world volumes are not yet captured by the volume light probes.

### Sphere Light Probes
Sphere light probes are now dynamic and get updated as they move.
Filtered versions for rough surfaces are now firefly-free. The look of the reflections can appear less "foggy" than in the previous EEVEE version.

## Volumes
World volumes are no longer limited to clipping distance, which means they can completely block sun lights and the world background. Older file can be converted using the conversion operator in the Help menu or in the `World > Volume` panel. (blender/blender!114062)

- Volume lighting is now dithered to avoid flickering.
- EEVEE now maximizes the depth range automatically if no world is present.
- Mesh objects now have correct volume intersection instead of rendering bounding boxes.
- Evaluation of many small volume objects has been optimized.

## World
Sun light contribution is now automatically extracted from HDRI lighting. [See the manual](https://docs.blender.org/manual/en/4.2/render/eevee/world_settings.html#sun).

## Image Stability
Improvements in viewport image stability through velocity-aware temporal supersampling should help reduce the noise and aliasing issues that were previously present.

Clamping options have been enhanced to help prevent fireflies and to better match Cycles. The properties remain independent for each renderer.

## Motion Blur
Motion blur is now partially supported in the viewport, through the camera view.

Shutter curves are now supported.


## Depth of Field
Depth of field was rewritten and optimized, removing the `Denoise Amount` and `High Quality Slight Defocus` settings which are now always on.

## User Interface
- The panels and properties have been adjusted to be closer to Cycles.
- The object `Display As` property set to `Wire` or `Bounds` will now render in `Render` viewport shading mode. (blender/blender@1f2b935146)
- Cycles and EEVEE now share the `Cast Shadow` property.

## Multithreaded Shader Compilation
There's a new `Max Shader Compilation Subprocesses` option in `Preferences > System > Memory & Limits` that allows using multiple subprocesses to speed-up shader compilation on Windows and Linux.

## Platform compatibility

There are some platforms that are currently not working as expected at the initial release. Improvements to hardware support will become available in future updates.

- Apple Intel iGPU can leave red spots when ray tracing is enabled. [blender/blender#122361]
- Windows on Intel HD 5000 series of GPU doesn't render dithered materials [blender/blender#122837]
- Linux Mesa AMD RX 7000 series crashes after rendering when motion blur is enabled [blender/blender#124044]
