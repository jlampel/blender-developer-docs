# Blender 4.2 LTS: Modeling & UV

## Transform

- Customizable rotation snap increment added for *Transform* tool
  (blender/blender@060174cf14).
- The 'Absolute Grid Snap' option has been replaced with a new snap mode: Snap to Grid.
  (blender/blender@f0479e915f). If combined with Snap to Increments it allows for 3D Volume Grid Snap. 
  
## Modifiers
- New option to pin modifiers to the bottom of the stack (blender/blender@7ee189416c)
- New operator, Shade Auto Smooth, to quickly add a Smooth by Angle modifier and pin it to the bottom of the stack (blender/blender@2f289857af)
- Moved Normal Edit and Weighted Normal modifiers to new Normals menu (blender/blender@b0eb48c4dc)

## UV Editing

- The Edge and Vert Slide operators have been ported to the UV Editor
  (blender/blender@aaadb5005e).
- The `Set Snap Base` feature (key `B` ) have been enabled in the UV Editor
  (blender/blender@a56a975760).

## Curves Editing

- Curves edit mode draws evaluated curves and handles now (blender/blender@15dbfe54e46).
- Support converting curves types (blender/blender@23265a2b6d16, blender/blender@3f2c4db95105).
- New *Add* menu to insert some primitive curves (blender/blender@549d02637af6).
- New operator to set the handle type for bezier curves (blender/blender@549d02637af6).
- New operator to subdivide curves (blender/blender@548df007a59a).
- New operator to switch direction of curves (blender/blender@6c25c66194d7).
- New operator to toggle whether curves are cyclic (blender/blender@d315a6a793d0).
- New option to only draw onto selected objects in "project to surface" draw mode (blender/blender@241c19a57a).
