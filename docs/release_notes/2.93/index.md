# Blender 2.93 Release Notes

Blender 2.93 was released on June 2, 2021.

Check out the final [release notes on
blender.org](https://www.blender.org/download/releases/2-93/).

This release includes long-term support, see the [LTS
page](https://www.blender.org/download/lts/) for a list of bugfixes
included in the latest version.

## [Geometry Nodes](geometry_nodes.md)

## [User Interface](user_interface.md)

## [Modeling](modeling.md)

## [Sculpt, Paint, Texture](sculpt.md)

## [Animation & Rigging](animation_rigging.md)

## [Grease Pencil](grease_pencil.md)

## [EEVEE & Viewport](eevee.md)

## [Render & Cycles](cycles.md)

## [Data Management, Linking, Overrides](core.md)

## [Pipeline, Assets & I/O](io.md)

## [Python API & Text Editor](python_api.md)

## [VFX & Video](vfx.md)

## [Nodes & Physics](physics.md)

## [Add-ons](add_ons.md)

## [More Features](more_features.md)

## Compatibility

Windows 7 is no longer supported. Windows 8.1 or newer is required.
[Microsoft discontinued Windows 7 support in January
2020](https://support.microsoft.com/en-us/windows/windows-7-support-ended-on-january-14-2020-b75d4580-2cc7-895a-2c9c-1466d9a53962).
