# Blender 2.93: User Interface

## Outliner

- The render visibility toggles are now shown by default.
  (blender/blender@510db9512fc6)
- When showing folded item, more sub-items types are gathered as a
  single icon + counter (all bones, bone groups, and vertex groups,
  blender/blender@92dfc8f2673e).

## File Browser

- "Frame Selected" operator to scroll to the selected files
  (blender/blender@5a67407d5aa880).
  This has shortcut `Numpad .` in Blender's default keymap, and `F`
  in the Industry Compatible keymap.

## Window Management

#### All Platforms

- Temporary windows play nicer with others. Opening a new window can no
  longer take over the space of a different window.
  (blender/blender@492e64c7bcbd).
- Window / "New Window" now opens with a simpler layout, with just a
  single editor (the largest of the parent).
  (blender/blender@be9842f65b85).

#### Windows Platform

- Child windows are shown on top of parent windows. You can now work
  with overlapping and floating windows.
  (blender/blender@261fa052ac42).
- Improved positioning of windows on multiple monitors.
  (blender/blender@d447bd3e4a9a)
  (blender/blender@97fabc3c1c92).
- Improvements to Full-screen support
  (blender/blender@12193035ed12)
  (blender/blender@9b87d3f02962).

## General Changes

- Transform arrow cursor improvements in shape and in response to line
  width changes.
  (blender/blender@b4b02eb4ff77).

![](../../images/TransformArrowComparison.png){style="width:500px;"}

- Navigation Gizmo changes. Better indication of negative axes,
  consistent use of color and size to indicate orientation, ability to
  be resized.
  (blender/blender@ded9484925ed).

![](../../images/NavigateGizmoHighlight.png){style="width:500px;"}

- Position Gizmo tooltips below their bounds so as to not obscure
  themselves.
  (blender/blender@cf6d17a6aa42).

![](../../images/GizmoTooltips.png){style="width:500px;"}

- Use ellipsis character when truncating strings no matter how narrow.
  (blender/blender@de3f369b30e5).
- Windows OS - Allow any input language, regardless of translated
  display language.
  (blender/blender@f2781e1c7c82).
- Improved contrast for Status Bar report messages.
  (blender/blender@694bc4d040a8).

![](../../images/Status_Bar_Reports.png){style="width:500px;"}

- Window Manager: Add 'Confirm On Release' option for `WM_OT_radial_control`
  (blender/blender@631cc5d56ee9445ece0a9982ed58b1de1dcbbe5b).
- Update layouts for graph editor and NLA editor FModifiers
  (blender/blender@1f5647c07d15d2).
- Blender thumbnails now saved without "Camera View"-style Passepartout.
  (blender/blender@9c395d6275a0).
- Virtual node sockets (unconnected node group sockets) are now more
  visible
  (blender/blender@b54b56fcbea62b).
- Add Add Copy Full Data Path to RMB menu
  (blender/blender@85623f6a5590).
- Dynamically increase width of data-block and search-menu name buttons
  for long names where there is enough space
  (blender/blender@2dd040a34953).
- Allow translation of strings even when in 'en_US' locale, useful for
  non-English add-ons
  (blender/blender@7a05ebf84b35).
