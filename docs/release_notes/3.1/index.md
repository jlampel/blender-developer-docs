# Blender 3.1 Release Notes

Blender 3.1 was released on March 9, 2022.

Check out the final [release notes on
blender.org](https://www.blender.org/download/releases/3-1/).

## [Animation & Rigging](animation_rigging.md)

## [Core](core.md)

## [EEVEE & Viewport](eevee.md)

## [Grease Pencil](grease_pencil.md)

## [Modeling](modeling.md)

## [Nodes & Physics](nodes_physics.md)

## [Pipeline, Assets & I/O](pipeline_assets_io.md)

## [Python API & Text Editor](python_api.md)

## [Render & Cycles](cycles.md)

## [Sculpt, Paint, Texture](sculpt.md)

## [User Interface](user_interface.md)

## [VFX & Video](vfx.md)

## [Add-ons](add_ons.md)

## Compatibility

See relevant sections in
[Add-Ons](add_ons.md#compatibility-issues).

## [Corrective Releases](corrective_releases.md)
